
cordova-plugin-file 6.0.1 "File"
cordova-plugin-market 1.2.0 "Market"
cordova-plugin-whitelist 1.3.3 "Whitelist"
cordova-plugin-x-socialsharing 5.6.4 "SocialSharing"
es6-promise-plugin 4.2.2 "Promise"


cordova plugin rm cordova-plugin-file
cordova plugin rm cordova-plugin-market
cordova plugin rm cordova-plugin-whitelist
cordova plugin rm cordova-plugin-x-socialsharing
cordova plugin rm es6-promise-plugin

cordova plugin add cordova-plugin-file
cordova plugin add cordova-plugin-market
cordova plugin add cordova-plugin-whitelist
cordova plugin add cordova-plugin-x-socialsharing
cordova plugin add es6-promise-plugin



cordova build --release android -- --minSdkVersion=19
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore Stutigaan-mobileapps.keystore platforms\android\build\outputs\apk\release\android-release-unsigned.apk Stutigaan-mobileapps
D:\Projects\androidsdk\build-tools\30.0.2\zipalign -v 4 platforms\android\build\outputs\apk\release\android-release-unsigned.apk release.apk

