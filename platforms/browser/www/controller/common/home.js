var commonhomeController = function() {
	this.view = "default";
	this.style = "default";
	this.dom = null;
	this.data = null;
};
commonhomeController.prototype.Init = function(viewDom)
{
	this.dom = viewDom;
	this.SetLayout();
	this.OnlineInit();
};
commonhomeController.prototype.SetLayout = function()
{
	var urlMgmt = UrlMgmt.getObj();
	urlMgmt.headerAction.controller.DisplayHeader(this.dom, 'Songs Book', true, false, false);
};
commonhomeController.prototype.OnlineInit = function() {
	$(this.dom).find('.item').each(function(key, value){
		var action = $(value).attr('data-action');
		var dataValue = $(value).attr('data-value');
		var url = 'img/Button_Svg/'+action+'.png';
		this.GetSongCount(action+".txt", function(count){
			var html = "<div><img src='"+url+"' alt='"+dataValue+"'/></div>";
			html += "<div class='item odd-p'><p class='songs-cat-name'>"+dataValue+"</p>"+
						"<p class='songs-cat-range'>"+count+" Songs</p>"+
					"</div>";
			$(value).html(html);
		});
		
	}.bind(this));
	this.BindEvents();
};
commonhomeController.prototype.RefreshView = function()
{
	var urlMgmt = UrlMgmt.getObj();
	urlMgmt.showLoader();
	this.data.header = urlMgmt.menuData;
	this.OnlineInit();
	urlMgmt.removeLoader();
};
commonhomeController.prototype.BindEvents = function()
{
	$(this.dom).find('.item').unbind().bind('click', function(e){
		var action = $(e.currentTarget).attr('data-action');
		var title =  $(e.currentTarget).attr('data-value');
		UrlMgmt.getObj().Route({route: "common/songlist", data:{title: title, filename: action + ".txt"}});
	});
};
commonhomeController.prototype.GetSongCount = function(filename, callback)
{
	$.ajax({
		url: filename,
		context: this,
		success: function (data){
			 console.log(filename);
            console.log(data.match(/sno-/g).length);
			
			callback(data.match(/sno-/g).length);
		}
	});
};