var commonsonglistController = function() {
	this.view = "default";
	this.style = "default";
	this.dom = null;
	this.data = null;
	this.songLines = [];
	this.listSize = 25;
	this.folderName = "";
	//Get it from server
	//this.s3Url = "https://stutigaan-songs.s3.ap-south-1.amazonaws.com/";
	this.s3Url = "https://christchurch.ind.in/Songs/";
	this.playing = false;
	this.songIndex = 0;
	this.songCount = 0;
	this.durationObj = null;
	this.currentSongTitle = "";
	this.onceDonePause = false;
	this.onceDonePlay = false;
	this.stopthesong = false;
	this.repeatsongs = false;
	this.randomSongPosition = 1;
	this.randomSongsPlay = false;
	this.songsPresent = [];
	this.randomSongSequence = [];
	this.songListfromServer = null;
};
commonsonglistController.prototype.Init = function(viewDom)
{
	this.dom = viewDom;
	this.SetLayout();
	this.OnlineInit();
};
commonsonglistController.prototype.SetLayout = function()
{
	var urlMgmt = UrlMgmt.getObj();
	console.log(this.data);
	var title = this.data.title;
	urlMgmt.headerAction.view.hide();
	//urlMgmt.headerAction.controller.DisplayHeader(this.dom, title, true, false, true);
};
commonsonglistController.prototype.OnlineInit = function() { 
	var imgName = "img/Button_Svg/" + this.data.filename.split(".")[0] + ".png";
	
	$(this.dom).find('.view-songlist .page-head').css('background-image', 'url("'+imgName+'")');
	$(this.dom).find('.view-songlist .menu-icon1 span.songlist-head').text(this.data.title);
	//this.Effect();
	
	//Get the folder name for audio play
	this.folderName = this.data.filename.split(".")[0];
	this.LoadSongs();
	this.getSongsListformServer();
};
commonsonglistController.prototype.RefreshView = function()
{
	var urlMgmt = UrlMgmt.getObj();
	urlMgmt.showLoader();
	this.data.header = urlMgmt.menuData;
	this.OnlineInit();
	urlMgmt.removeLoader();
};
commonsonglistController.prototype.BindEvents = function()
{
	$(this.dom).find('.songlist').find('div.playsong').unbind().bind('click', function(e){
		var songId = ($(e.currentTarget).attr('data-action')).split('_')[1];
		UrlMgmt.getObj().modalObj = songId;
		//Play the song in the modal
		this.songId = songId;
		this.PlaySong();
	}.bind(this));
	
	$(this.dom).find('div.song_action').unbind().bind('click', function(e){
		if($(e.currentTarget).hasClass("loaded")){
			this.PlayPauseSong($(e.currentTarget),true);
		}
		else{
			this.playing = false;
			this.PlayAudio($(e.currentTarget));
		}
	}.bind(this));
	
	$(this.dom).find('.header-back-btn').unbind().bind('click', function(){
		UrlMgmt.getObj().PopView();
	});
	//By deafult, open first panel
	$('.songlist').find('.panel:first-child').find('.panel-heading').find('img').css("transform", "rotate(180deg)");
	$('.panel').on('hidden.bs.collapse', function (e) {
		$(e.currentTarget).find('.panel-heading').find('img').attr('src', 'img/down-arrow.svg');
		$(e.currentTarget).find('.panel-heading').find('img').css("transform", "rotate(0deg)");
	});
	$('.panel').on('show.bs.collapse', function (e) {
		$(e.currentTarget).find('.panel-heading').find('img').css("transform", "rotate(180deg)");
	});
	$('#song_dialog .playerIcon').unbind().bind('click', function () {
		// console.log("You clicked !!!");
		this.PlayAudio($(this.dom).find('div.song_action[data-action=song_' + this.songId + ']'));
		console.log("this.songIndex",this.songIndex);
		console.log("this.songId", this.songId);
		console.log("this.playing", this.playing);
		// console.log();
		// this.PlaySong();
	}.bind(this));	
	$("#song_dialog  .backIcon").unbind().bind('click', function () { 
		$("#song_dialog .dialog-header .action").click();
	});
	this.SongRandomSequenceGeneration();
};
commonsonglistController.prototype.LoadSongs = function()
{
	$.ajax({
		url: this.data.filename,
		context: this,
		success: function (data){
			var line = '';
			var songFound = false;
			var sno = 0;
			var html = "";
			var continueSong = false;
			for(var i=0;i<data.length;i++){	
				if(data[i] == '\n'){
					this.songLines.push(line.trim());
					line = "";
				}
				line += data[i];
			}
			for(var i in this.songLines){
				if(songFound == true){
					html += "<div data-action='song_"+sno+"' data-songpresent='false' class='playsong song_"+sno+"'><h4>"+sno+"</h4><pre>"+(this.songLines[i]).trim()+"</pre></div>";
					songFound = false;
				}
				else if(this.songLines[i].startsWith('sno') || this.songLines[i].startsWith(' sno')){
					songFound = true;
					sno = this.songLines[i].split('-')[1]; 
				}
				else{
					continue;
				}
			}
			$(this.dom).find('.dummy-data').html(html);
			var count = 0;
			var start = 1;
			var end = this.listSize;
			var finalHtml  = "";
			var totalSongs = $(this.dom).find('div.playsong').length;
			var first = false;
			$(this.dom).find('div.playsong').each(function(key, value){
				if(count%this.listSize == 0){
					finalHtml += '<div class="panel panel-default">'+
									'<div class="panel-heading">'+
										'<h4 class="panel-title">'+
											'<a data-toggle="collapse" data-parent="#accordion" href="#collapse'+count+'">Songs '+start+' - '+end+'<span class=""><img src="img/down-arrow.svg"></span></a>'+
										'</h4>'+
									'</div>';
									if(first == false){
										finalHtml += '<div id="collapse'+count+'" class="panel-collapse collapse in">';
										first = true;
									}
									else{
										finalHtml += '<div id="collapse'+count+'" class="panel-collapse collapse">';
									}
									finalHtml += '<div class="panel-body">';
					start += this.listSize;
					end += this.listSize;
					if(totalSongs < end){
						end = totalSongs;
					}
				}
				finalHtml += "<div class='song_div'>";
				finalHtml += "<div data-dialog='song_dialog' data-action='song_" + (key + 1) + "' data-songpresent='false' class='trigger playsong song_"+(key+1)+"'>"+$(value).html()+"</div>";
				finalHtml += "<div class='song_action' data-action='song_" + (key + 1) +"'><img data-action='playsong' src='img/Songplay/playbutton_listpage.svg'/></div>";
				finalHtml += "</div>";
				finalHtml += "<div class='bottom-border'></div>";
				count++;
				if(count%this.listSize == 0){
					finalHtml += '</div>'+
									'</div>'+
									'</div>';
				}
			}.bind(this));
			this.songCount = count;
			$(this.dom).find('.songlist').find('.panel-group').html(finalHtml);
			$(this.dom).find('div.playsong.trigger').each(function(key, value){
				this.Effect(value, key);
			}.bind(this));
			
			
			//this.Effect();
			$(this.dom).find('.dummy-data').remove();
			this.BindEvents();
		}.bind(this)
	});
};
commonsonglistController.prototype.PlaySong = function()
{
	var songFound = false;
			
	var htmlData = "<div class='song-text'>";
	for(var i in this.songLines){
		if(songFound == true && !this.songLines[i].startsWith('--endofsong--')){
			htmlData += "<pre>"+(this.songLines[i]).trim()+"</pre>";
		}
		else if(songFound == true && this.songLines[i].startsWith('--endofsong--')){
			htmlData += "</div>";
			songFound = false;
			break;
		}
		else if(this.songLines[i].startsWith('sno')){
			var id = this.songLines[i].split('-')[1];
			if(id == this.songId){
				songFound = true;
			}					
		}
		else{
			continue;
		}
	}
	$(song_dialog).find('.dialog-inner').find('.dialog-title').html(this.songId);
	$('#song_dialog').find('.dialog-inner').find('.dialog-body').html(htmlData);
	
};
commonsonglistController.prototype.Effect = function(targetEvent, firstTime)
{
	var sendEvent = false;
	if(firstTime == 0){
		sendEvent = true;
	}
	var dlgtrigger = targetEvent,
		somedialog = document.getElementById( dlgtrigger.getAttribute( 'data-dialog' ) ),
		morphEl = somedialog.querySelector( '.morph-shape' ),
		s = Snap( morphEl.querySelector( 'svg' ) ),
		path = s.select( 'path' ),
		steps = { 
			open : morphEl.getAttribute( 'data-morph-open' ),
			close : morphEl.getAttribute( 'data-morph-close' )
		},
		dlg = new DialogFx(sendEvent, somedialog, {
			onOpenDialog : function( instance ) {
				// animate path
				setTimeout(function() {
					path.stop().animate( { 'path' : steps.open }, 1500, mina.elastic );
				}, 150 );
			},
			onCloseDialog : function( instance ) {
				UrlMgmt.getObj.modalObj = null;
				path.stop().animate( { 'path' : steps.close }, 250, mina.easeout );
			}
		});
	dlgtrigger.removeEventListener('click', dlg);
	dlgtrigger.addEventListener( 'click', dlg.toggle.bind(dlg) );
}


/* Audio play code starts */
commonsonglistController.prototype.PlayAudio = function(e)
{
	$('.song_action').removeClass("loaded");
	$(e).addClass("loaded");
	$('.song_action').find('img').attr('src', 'img/Songplay/playbutton_listpage.svg');
	$('#song_audio_play').find('.control.next').removeClass('disabled');
	$('#song_audio_play').find('.control.previous').removeClass('disabled');
	var songId = ($(e).attr('data-action').split("_"))[1];
	this.songIndex = songId; // song_1
	
	this.audioPath = this.s3Url + this.folderName + "/audio-" + this.songIndex + ".mp3";// + ".mp3";
	//this.audioPath = "http://christchurch.ind.in/Songs/English%20Songs%20Mp3/1.%20He%27s%20Still%20Working%20on%20Me.mp3"
	if(this.songIndex == 1){
		$('#song_audio_play').find('.control.previous').addClass('disabled');
	}
	else if(this.songIndex == this.songCount){
		$('#song_audio_play').find('.control.next').addClass('disabled');
	}
	//Song Title, TODO: remove any tags and trim
	this.currentSongTitle = $(e).siblings('.playsong').find('pre').html();
	$('#song_audio_play').find('.song-title').html($(e).siblings('.playsong').find('pre').html());
	$('#song_audio_play').find('#audioSong').attr('src', this.audioPath);
	try{
		this.PlayPauseSong(e, true);
	}
	catch(e){
		alert(e);
	}
	this.durationObj = setInterval(function(){
		this.SetSongDuration();
		this.UpdateProgressValue(e);
	}.bind(this), 1000);
	this.AudioBindEvents(e);
};


//Music Player
commonsonglistController.prototype.PlayPauseSong = function(e, openModal = false)
{
	var self = this;
	try{
		var audioSong = document.querySelector('#audioSong');
		if (!this.playing) {
		    // Play the audio, change the play icon to pause
			var playPromise = audioSong.play();
			if (playPromise !== undefined) {
			  playPromise.then(function(success) {
				  if(openModal){
					  $('#song_audio_play').modal('show');
				  }
				  console.log("Audio duration: "+audioSong.duration);
				  setTimeout(function(){
					  this.SetSongTotalDuration();  
				  }.bind(this), 1000);
				  
				  this.playing = true;
				  $('#song_audio_play').find('.control.playPause').find('img').attr('src', 'img/Songplay/player_pause_button.svg');
				  $('#song_audio_play').find('.control.playPause').find('img').attr('songplaying', 'true');
				  $('#song_audio_play .minimizedDiv').find('.control.playPause').find('img').attr('src', 'img/Songplay/pausebutton_listpage.svg');
				//   pausebutton_listpage.svg
				  $('#song_audio_play .minimizedDiv').find('.control.playPause').find('img').attr('songplaying','true');
				  $('#song_dialog').find(".playerIcon img").attr('songplaying', 'true');
				  $('#song_dialog').find(".playerIcon img").attr('src', 'img/Songplay/Pause.png');
				  $(e).find('img').attr('src', 'img/Songplay/pausebutton_listpage.svg');
				  
				  //Notification Add
				  cordova.plugins.notification.local.cancelAll(function(){
					  cordova.plugins.notification.local.schedule({
					    title: self.currentSongTitle,
					    actions: [{ id: 'pause', title: 'Pause' }]
					});
				  });
				  if(!this.onceDonePause){
					  this.onceDonePause = true;
					  //on('click') - to trigger event on notification click
					  cordova.plugins.notification.local.on('pause', function(notification, eopts) {
						  console.log("Inside notification Pause click");
						  self.PlayPauseSong(e,true);
					  });
				  }
			  }.bind(this)).catch(function(error) {
				  console.log("Error: "+error);
				  $('#song_audio_play').modal('hide');
				  $('.song_action').find('img').attr('src', 'img/Songplay/playbutton_listpage.svg');
				  alert("Song Audio Not available");
			  });
			}
	   }
	   else {
		  // Pause the audio, change the pause icon to play
		  audioSong.pause();
		  this.playing = false;
			$('#song_audio_play').find('.control.playPause').find('img').attr('src', 'img/Songplay/playbutton_minimizedplayer.svg');
			$('#song_audio_play').find('.control.playPause').find('img').attr('songplaying', 'false');
			$('#song_audio_play .minimizedDiv').find('.control.playPause').find('img').attr('src', 'img/Songplay/playbutton_minimizedplayer.svg');
			$('#song_audio_play .minimizedDiv').find('.control.playPause').find('img').attr('songplaying', 'false');
			$(e).find('img').attr('src', 'img/Songplay/playbutton_listpage.svg');
			$('#song_dialog').find(".playerIcon img").attr('songplaying', 'true');
			$('#song_dialog').find(".playerIcon img").attr('src', 'img/Songplay/playbutton_minimizedplayer.svg');
		  //Notification Add
		  cordova.plugins.notification.local.cancelAll(function(){
			  cordova.plugins.notification.local.schedule({
			    title: self.currentSongTitle,
			    actions: [{ id: 'play', title: 'Play' }]
			});
		  });
		  if(!this.onceDonePlay){
			  this.onceDonePlay = true;
			  cordova.plugins.notification.local.on('play', function(notification, eopts) {
				  console.log("Inside notification play event");
				  self.PlayPauseSong(e,true);
			  });
		  }
	   }
	}
	catch(e)
	{
		alert("error");
		alert(e);
	}
};

commonsonglistController.prototype.NextSong = function()
{
	this.songIndex++;
	this.playing = false;
	this.PlayAudio($(this.dom).find('div.song_action[data-action=song_'+this.songIndex+']'));
}

commonsonglistController.prototype.PreviousSong = function()
{
	this.songIndex--;
	this.playing = false;
	this.PlayAudio($(this.dom).find('div.song_action[data-action=song_'+this.songIndex+']'));
}

commonsonglistController.prototype.UpdateProgressValue = function(e) {
	var progressBar = document.querySelector('#newSlider');
	var audioSong = document.querySelector('#audioSong');
	var range2 = $("#newSlider");
	progressBar.max = audioSong.duration;
	var progressBar2 = $(".progressslider");
	$(range2).text($(range2).val() + "%");
	progressBar2.css("width", (parseFloat($(range2).val()) * 100) / parseFloat($(range2).attr("max")) + "%");
	progressBar.value = audioSong.currentTime;
	if(audioSong.currentTime == audioSong.duration){
		if(this.durationObj != null){
			this.durationObj = null;
			clearInterval(this.durationObj);
		}
		this.playing = false;
		$('#song_audio_play').find('.control.playPause').find('img').attr('src', 'img/Songplay/playbutton_minimizedplayer.svg');
		$('#song_audio_play').find('.control.playPause').find('img').attr('songplaying', 'false');
		if ($('#song_audio_play').find(".control.next.disabled").length == 0)
		{
			if (this.randomSongsPlay == true) {
				if (this.randomSongSequence.length == this.randomSongPosition && this.randomSongsPlay == true)
				{	
					this.randomSongPosition=1;
				}
				else if (this.randomSongSequence.length == this.randomSongPosition){
					$('#song_audio_play .minimizedDiv').find('.control.playPause').find('img').attr('songplaying', 'false');
					$(e).find('img').attr('src', 'img/Songplay/playbutton_listpage.svg');
					return;
				}	
				this.songIndex = this.randomSongSequence[this.randomSongPosition++];
				this.playing = false;
				this.PlayAudio($(this.dom).find('div.song_action[data-action=song_' + this.songIndex + ']'));
			}
			else{
				$('#song_audio_play').find('.control.next').click();
			}
			
		}
		else{
			if (this.repeatsongs == true) {
				
				if (this.randomSongsPlay == true){
					if (this.randomSongSequence.length == this.randomSongPosition)
						this.randomSongPosition = 1;
					this.songIndex = this.randomSongSequence[this.randomSongPosition++];
					this.playing = false;
					this.PlayAudio($(this.dom).find('div.song_action[data-action=song_' + this.songIndex + ']'));
				}
				else{
					this.songIndex = 1;
					this.playing = false;
					this.PlayAudio($(this.dom).find('div.song_action[data-action=song_' + this.songIndex + ']'));
				}
			}
		}
		$('#song_audio_play .minimizedDiv').find('.control.playPause').find('img').attr('songplaying', 'false');
		$(e).find('img').attr('src', 'img/Songplay/playbutton_listpage.svg');
	}
};
commonsonglistController.prototype.SetSongDuration = function(){
	var audioSong = document.querySelector('#audioSong');
	var duration = this.GetFormattedTime(audioSong.currentTime);
	$('.song_audio_play').find('.songDuration').find('.currentTime').html(duration);
};
commonsonglistController.prototype.SetSongTotalDuration = function(){
	var audioSong = document.querySelector('#audioSong');
	var duration = this.GetFormattedTime(audioSong.duration);
	
	$('.song_audio_play').find('.songDuration').find('.totalTime').html(duration);
};
commonsonglistController.prototype.GetFormattedTime = function(duration){
	var hrs = parseInt(duration/3600);
	var mins = 0;
	var secs = 0;
	if(hrs > 0){
		var temp = parseInt(duration%3600);
		mins = parseInt(temp/60);
		if(mins > 0){
			secs = parseInt(temp%60);
		}
	}
	else{
		mins = parseInt(duration/60);
		secs = parseInt(duration%60);
	}
	hrs = (hrs > 0 && hrs < 10) ? '0'+hrs : hrs;
	mins = mins < 10 ? '0'+mins : mins;
	secs = secs < 10 ? '0'+secs : secs;
	
	var text = hrs != 0 ? hrs+":" : "";
	text += mins + ":"+secs;
	return text;
}

commonsonglistController.prototype.HandleMinimizingAudioPlay = function(e){
	$('#song_audio_play').addClass('minimize');
	if(!$('#song_audio_play').hasClass('in')){
		$('#song_audio_play').modal('show');
		this.AudioBindEvents(e);
	}
	$('.modal-backdrop').hide();
	$('#song_audio_play').find('.normalDiv').removeClass('show').addClass('hide');
	$('#song_audio_play').find('.minimizedDiv').removeClass('hide');
	$('#song_audio_play').find('.minimizedDiv').css('display', 'flex');
};

commonsonglistController.prototype.HandleMaxizingAudioPlay = function(e){
	$('#song_audio_play').removeClass('minimize');
	if(!$('#song_audio_play').hasClass('in')){
		$('#song_audio_play').modal('show');
		this.AudioBindEvents(e);
	}
	$('.modal-backdrop').show();
	$('#song_audio_play').find('.minimizedDiv').removeClass('show').addClass('hide');
	$('#song_audio_play').find('.minimizedDiv').css('display', 'none');
	$('#song_audio_play').find('.normalDiv').removeClass('hide').addClass('show');
};
commonsonglistController.prototype.AudioBindEvents = function(e){
	$("#song_audio_play").unbind();
	var range2 = $("#newSlider");
	var progressBar2 = $(".progressslider");
	$(range2).on("input change", function () {
		progressBar2.css("width", (parseFloat($(this).val()) * 100) / parseFloat($(this).attr("max")) + "%");
		console.log(this)
		$(this).text($(this).val() + "%");
		var audioSong = document.querySelector('#audioSong');
		var progressBar = document.querySelector('#newSlider');
		audioSong.currentTime = progressBar.value;
	});
	// $('#song_audio_play').find('#progress-bar').unbind().bind('change', function(){
	// 	var audioSong = document.querySelector('#audioSong');
	// 	var progressBar = document.querySelector('#progress-bar');
	// 	audioSong.currentTime = progressBar.value;	
	// });
	$('#song_audio_play').find('.control.playPause').unbind().bind('click', function(){
		this.PlayPauseSong(e,true);
	}.bind(this));
	$('#song_audio_play').find('.control.playclose').unbind().bind('click',this, function (e) {
		// minimize - option
		$(this).closest(".modal-content").find(".modalheading .minimize-option").click();
		if($('#song_audio_play .minimizedDiv').find('.control.playPause').find('img').attr('songplaying') == "true"){
			$(this).closest(".controls").find(".control.playPause").click();
		}
		else{
			
		}
		
		e.data.stopthesong = true;
		$('#song_audio_play').modal('hide');
	});
	$('#song_audio_play').find('.control.previous').unbind().bind('click', function(){
		if (this.randomSongsPlay == true) {
			if (this.randomSongSequence.length == this.randomSongPosition)
				this.randomSongPosition = 1;
			this.songIndex = this.randomSongSequence[this.randomSongPosition++];
			this.playing = false;
			this.PlayAudio($(this.dom).find('div.song_action[data-action=song_' + this.songIndex + ']'));
			return;
		}
		this.PreviousSong();
	}.bind(this));
	$('#song_audio_play').find('.control.next').unbind().bind('click', function(){
		if (this.randomSongsPlay == true) {
			if (this.randomSongSequence.length == this.randomSongPosition)
				this.randomSongPosition = 1;
			this.songIndex = this.randomSongSequence[this.randomSongPosition++];
			this.playing = false;
			this.PlayAudio($(this.dom).find('div.song_action[data-action=song_' + this.songIndex + ']'));
			return;
		}
		this.NextSong();
	}.bind(this));
	$('#song_audio_play').find('.control.repeat').unbind().bind('click', function () {

		if(this.repeatsongs == true)
		{
			this.repeatsongs = false;
			$('#song_audio_play').find('.control.repeat img').attr('src', 'img/Songplay/player_repeatsongs_notactive.svg');
		}		
		else{
			this.repeatsongs = true;
			$('#song_audio_play').find('.control.repeat img').attr('src', 'img/Songplay/player_repeatsongs_active.svg');
		}
			
		console.log("repeatsong has been set ", this.repeatsongs);
	}.bind(this));
	$('#song_audio_play').find('.control.shuffel').unbind().bind('click', function () {
		
		if (this.randomSongsPlay == true)
		{
			this.randomSongsPlay = false;
			$('#song_audio_play').find('.control.shuffel img').attr('src', 'img/Songplay/player_shuffel_notactive.svg');
		}	
		else{
			this.randomSongsPlay = true;
			$('#song_audio_play').find('.control.shuffel img').attr('src', 'img/Songplay/player_shuffel_active.svg');
		}
			
		console.log("randomSongsPlay has been set ", this.randomSongsPlay);	
	}.bind(this));
	$('#song_audio_play').find('.minimize-option').unbind().bind('click', function(modal){
		if($('#song_audio_play').hasClass('minimize')){
			this.HandleMaxizingAudioPlay(e);
		}
		else{
			
			this.HandleMinimizingAudioPlay(e);
		}
	}.bind(this));
	$('#song_audio_play .modal-body ').find('.song-title').unbind().bind('click', function (modal) {
		if ($('#song_audio_play').hasClass('minimize')) {
			this.HandleMaxizingAudioPlay(e);
		}
		else {
			return;
		}
	}.bind(this));
	$("#song_audio_play").on('hidden.bs.modal', function(){
		if (this.stopthesong){
			this.stopthesong = false;
			return;
		}
		if($('#song_audio_play').hasClass('minimize')){
			this.HandleMaxizingAudioPlay(e);
		}
		else{
			this.HandleMinimizingAudioPlay(e);
		}
	}.bind(this));
}

commonsonglistController.prototype.SongRandomSequenceGeneration = function(){
	// Song Sequence must be generated after the song present ajax call is trigered
	$(".song_div [data-songpresent=true]").each(function (index, item) {
		this.songsPresent.push(parseInt(($(item).attr('data-action').split("_"))[1]));
	}.bind(this));	
	console.log("this.songsPresent", this.songsPresent);
	
	var randno = 0;
	while (this.randomSongSequence.length < this.songsPresent.length) {
		randno = Math.floor(Math.random() * this.songsPresent.length);
		if (!this.randomSongSequence.includes(parseInt(this.songsPresent[randno])))
			this.randomSongSequence.push(parseInt(this.songsPresent[randno]))
	}
	console.log("this.randomSongSequence", this.randomSongSequence);
}
commonsonglistController.prototype.getSongsListformServer = async function (){
	const proxyurl = "https://cors-anywhere.herokuapp.com/";
	const url = "https://christchurch.ind.in/Songs/songs.json"; // site that doesn’t send Access-Control-*
	// var json1 = await fetch(proxyurl + url) // https://cors-anywhere.herokuapp.com/https://example.com
	// 	.then(function (response){
	// 		var jasondata =  JSON.parse(response.text());
	// 		// this.songListfromServer = JSON.parse(response.text())
	// 		// this.disableSongsWhichIsNotPresetInServer();
	// 	} )
	// 	.catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"))
	// this.songListfromServer = JSON.parse(json1);
	$.ajax({
		type: "GET",
		url: "https://cors-anywhere.herokuapp.com/" + "https://christchurch.ind.in/Songs/songs.json",
		success: function (msg) {
			// console.log(msg);
			this.songListfromServer = msg;
			this.disableSongsWhichIsNotPresetInServer();
		}.bind(this),
		error: function (msg) {
			// OnError()
			console.log("error", JSON.parse(msg.responseText))
		}
	});
};
commonsonglistController.prototype.disableSongsWhichIsNotPresetInServer = function () {
	console.log("this.folderName=>",this.folderName);
	console.log("this.songListfromServer=>", this.songListfromServer);
	console.log("this.songListfromServer[this.folderName]", this.songListfromServer[this.folderName]);
	$.each(this.songListfromServer[this.folderName], function (i, item) {
		$("#commonsonglistController .song_div").find('[data-action="song_'+item.split("-")[1]+'"]').attr("data-songpresent", "true");
	});
	$("#commonsonglistController .song_div").find('[data-songpresent="false"]').closest(".song_div").find(".song_action").remove();
	// $("#commonsonglistController .song_div").find('[data-action="song_"' + +']').attr("data-songpresent","true");
};	