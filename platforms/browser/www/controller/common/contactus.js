var commoncontactusController = function() {
	this.view = "default";
	this.style = "default";
	this.dom = null;
	this.data = null;
};
commoncontactusController.prototype.Init = function(viewDom)
{
	this.dom = viewDom;
	this.SetLayout();
	this.BindEvents();
};
commoncontactusController.prototype.SetLayout = function()
{
	var urlMgmt = UrlMgmt.getObj();
	urlMgmt.headerAction.view.hide();
};
commoncontactusController.prototype.BindEvents = function()
{
	$(this.dom).find('.header-back-btn').unbind().bind('click', function(){
		UrlMgmt.getObj().PopView();
	});
	$(this.dom).find(".item.whatsapp").unbind().bind('click',function(){
		window.open('https://chat.whatsapp.com/HWd0rCUqxKeHURNS9llV4u', '_system');
	});
	$(this.dom).find(".item.confernece").unbind().bind('click', function () {
		window.open('https://join.freeconferencecall.com/christchurchministries', '_system');
	});
	
	
}