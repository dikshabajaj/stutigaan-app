sno-1
<b>Praise the Lord <br></b>
Praise the lord from the heaven<br>
All the angels sing praise the lord<br>
Praise the lord<br>
From the hearts of creations<br>
We shall praise the name of the lord<br>
He commanded them<br>
and the heavens were made<br>
He established them<br>
and they won't pass away the sun and moon<br>
Praise the lord with their light<br>
All the stars up above they<br>
<br>

--endofsong--

sno-2
<b>We are gathering together unto Him (2)<br></b>
Unto Him shall the gathering of the people be<br>
1. We are gathering together unto Him <br>
2. We are praising together unto Him<br>
3. We are singing together unto Him<br>
<br>

--endofsong--

sno-3
<b>Rejoice in the Lord always<br></b>
and again I say rejoice(2), Rejoice, rejoice,<br>
and again I say rejoice(2)<br>
<br>

--endofsong--

sno-4
<b>Jehovah - Jireh, my provider<br></b>
His grace is sufcient for me(2)<br>
My God shall supply allmy needs,<br>
According to His riches in glory,<br>
He will giveHis angels charge over me,<br>
Jehovah - Jireh cares forme, forme, forme,<br>
Jehovah - Jirch cares forme .<br>
<br>


--endofsong--

sno-5
<b>For I'm building a people of power<br></b>
And I'm making people of praise,<br>
Who will move through this land by My Spirit,<br>
And will glorify My precious name<br>
O build your church,<br>
Lord ! Make it strong, Lord !<br>
Join our hearts. Lord ! Through Your Son,<br>
Make us one Lord, in Your body,<br>
In the kingdom of Your Son<br>
<br>
--endofsong--

sno-6
<b>Rejoice evermore<br></b>
For this is the will of God ( 3 )<br>
1. In Christ Jesus concerning you .<br>
2. Pray without ceasing<br>
3. In everything give thanks<br>
4. Quench not the Spirit<br>
<br>

--endofsong--

sno-7
<b>Praise him, praise him<br></b>
Praise Him, in the morning,<br>
Praise Him, in the noontime,<br>
Praise Him, praise Him,<br>
Praise Him, when the sun goes down<br>
2. Love Him<br>
3. ServeHim<br>
4. MeetHim<br>
5. Give Him<br>

--endofsong--

sno-8
<b>I've got peace like a river (2)<br></b>
I've got peace like a river, in my soul<br>
2. I've gotjoy like a fountain(2)<br>
3. I've got love like an ocean(2)<br>
4. I've got Jesus, Jesus, Jesus(2)<br>
<br>
--endofsong--


sno-9
<b>He's able, He 's able,  I know my Lord is able<br></b>
To carry me through,<br>
He heals the broken hearted, And sets the captives free<br>
He makes the lame to walk again<br>
And cause the blind to see.<br>
For He is able<br>
<br>
--endofsong--


sno-10
<b>When the saints go marching on (2)<br></b>
Lord I want to be among the numbered<br>
When the saints gomarching on<br>
<br>
--endofsong--

sno-11
<b>I'm so glad, I belong to Jesus (3) <br></b>
Alleluia, praise the Lord ! Praise the Lord, amen<br>
amen (6)<br>
Alleluia, praise the Lord<br>
<br>

--endofsong--

sno-12
<b>From the rising of the sun<br></b>
To the going down of the same<br>
The name of the Lord is to be praised,<br>
Praise Him all ye servants of the Lord,<br>
Praise the name of the Lord,<br>
Blessed be the name of the Lord,<br>
From this time forth and ever more.<br>
--endofsong--

sno-13
<b>He poured in me oil and the wine<br></b>
The kind that restoreth my soul<br>
He found me bleeding and dying<br>
On the Jericho road<br>
He poured in me oil and the wine.<br>
Jesus (3) I've got Him on my mind (3)<br>
I have Jesus on my mind.<br>
<br>
--endofsong--

sno-14
<b>Thy loving kindness is better than life (2)<br></b>
My lips shall praise Thee<br>
Thus will I bless thee<br>
The loving kindness is better than life<br>
I lift my hands up unto thy name (2)<br>
My lips shall praise thee <br>
<br>
--endofsong--

sno-15
<b>All over the world, the Spirit is moving,<br></b>
All over the world,<br>
As the prophets said it would be, Hallelujah<br>
All over the world, Ther ' s a mighty revelation,<br>
Of the Glory of the Lord, as the waters cover the sea<br>
1. Deep down in my heart<br>
2. Right here in our midst<br>
<br>
--endofsong--


sno-16
<b>I've found a new way of living<br></b>
I've found a new life divine,<br>
I've found the fruit of the Spirit,<br>
By abiding, abiding in the vine,<br>
Abiding in the vine, abiding in the vine<br>
Love, joy, health, peace,<br>
He had made them mine.<br>
I've found prosperity, power and victory,<br>
By abiding, abiding in the vine.<br>
<br>
--endofsong--


sno-17
<b>The Lord is my shepherd, I will walk with him always<br></b>
He knows me and loves me, I'll walk with him always<br>
Always, always, I'll walk with him always<br>
Always, always, I'll walk with him always<br>
<br>
--endofsong--

sno-18
<b>O Heavenly father … O light of the world<br></b>
O Heavenly father  O light of the world<br>
O heavenly father … O light of the world<br>
I will seek you in the morning and learn to walk inyour ways,<br>
Step by step go where you lead me, and love you all my days<br>
O heavenly father … O light of the world<br>
<br>
--endofsong--

sno-19
<b>Wonderful, wonderful, Jesus is to me<br></b>
Counsellor, mighty God, prince of peace is he<br>
Saving me, keeping me<br>
From my sin and shame<br>
He's the everything father<br>
Praise his name<br>
<br>
--endofsong--


sno-20
<b>Building up the temple, building up the temple <br></b>
Building up the temple of the Lord.<br>
Girls, come and help us.<br>
Boys, came and help us.<br>
Building up the temple of the Lord.<br>
<br>
--endofsong--

sno-21
<b>Put on the armour, put on the armour<br></b>
Put on the armour of God<br>
Be strong in the Lord<br>
And the power of his might<br>
And that you may withstand temptation<br>

<br>
--endofsong--

sno-22
<b>Open your heart, open your heart<br>
Open your heart to Jesus<br>
He knocks today, do not delay<br>
But open your heart to Jesus<br></b>
<br>
1. Come to my heart, come to my heart<br>
Come to my heart Lord Jesus<br>
Come in today, come in to say<br>
Come into my heart Lord Jesus<br>
<br>
2. He's in my heart, He is in my heart<br>
He's in my heart for ever<br>
He'll never go, this I know<br>
He's in my heart for ever<br>
<br>
--endofsong--