var _sqliteOps = null;
var SQLiteOps = function()
{
	this.db = null;
	this.databaseOpen = true;
	this.Init();
};
SQLiteOps.getObj = function()
{
	if(_sqliteOps === null)
	{
		_sqliteOps = new SQLiteOps();
	}
	return _sqliteOps;
};

SQLiteOps.prototype.Init = function(){
	if(typeof window.sqlitePlugin == 'undefined')
	{
		return;
	}
	this.db = window.sqlitePlugin.openDatabase({
		name : 'sample.db',
		location : 'default'
	}, function(database){
		console.log("Successfully opened database");
		this.databaseOpen = true;
	}.bind(this), function(error){
		console.log("Error in opening database");
		this.databaseOpen = false;
	}.bind(this));
};
function PrintLog(action, tableName, message){
	console.log("SQLITE:"+new Date().toLocaleString()+": Action:"+action+" , TableName:"+tableName+" , Message:"+message);
}
SQLiteOps.prototype.CreateTable = function(tableName, callback) {
	var query = "Create table if not exists " + tableName + "(id INTEGER PRIMARY KEY AUTOINCREMENT, "+
			"item_id INTEGER, data TEXT)";

	this.db.transaction(function(tx) {
		tx.executeSql(query);
	}, function(error) {
		PrintLog("Table creation", tableName, error.message);
		callback(false);
	}, function() {
		PrintLog("Table creation", tableName, "Successfully created table");
		/*
		db.transaction(function(tx) {
					tx.executeSql("CREATE INDEX "+tableName+"_id ON "+tableName+" (id);");
				}, function(error) {
					PrintLog("Index creation", tableName, error.message);
					callback(false);
				}, function() {
					PrintLog("Index creation", tableName, "Successfully created Index");
					callback(true);
				});*/
		
		callback(true);
	});
};
SQLiteOps.prototype.WriteToSQLite = function(tableName, itemId, data, callback) {
	var query = "";
	this.CreateTable(tableName, function(result) {
		if (result) {
			//Success
			this.GetData(tableName, itemId, function(result1) {
				if (result1 > 0) {
					//record already exists, update
					query = "Update " + tableName + " SET data = ? where item_id = ?";
					this.UpdateItem(query, tableName, itemId, data, function(){
						if(typeof callback == 'function'){
							callback();
						}
					});
				} else {
					//insert a new record
					query = "INSERT INTO " + tableName + " (item_id, data) VALUES (?,?)";
					this.InsertItem(query, tableName, itemId, data, function(){
						if(typeof callback == 'function'){
							callback();
						}
					});
				}
			}.bind(this));
		} else {
			//Error in creating table
			PrintLog("Write to SQLite", tableName, "Error in creating table");
			callback();
		}
	}.bind(this));
};

SQLiteOps.prototype.GetData = function(tableName, itemId, callback) {
	var query = "Select data from " + tableName + " where item_id = ?";
	
	this.db.transaction(function(tx) {
		tx.executeSql(query, [itemId], function(tx, resultSet) {
			callback(resultSet.rows.length);
		}, function(tx, error) {
			PrintLog("Select Data", tableName, error);
			callback(0);
		});
	}, function(error) {
		PrintLog("Select Data", tableName, error);
	}, function() {
		PrintLog("Select Data", tableName, "success");
	});
};

SQLiteOps.prototype.RemoveAllTables = function() {
	var query = 'SELECT name FROM sqlite_master WHERE type="table"';
	this.db.transaction(function(tx) {
		tx.executeSql(query, [], function(tx, rs) {
			for(var i=0;i<rs.rows.length;i++){
				if(rs.rows.item(i).name == 'firstlaunch' || rs.rows.item(i).name == 'Entity_config_userinfo'){
				}
				else{
					this.DropTable(rs.rows.item(i).name);
				}	
			}
		}.bind(this), function(tx, error) {
			PrintLog("Remove all tables", null, error);
		}.bind(this));
	}.bind(this), function(error) {
		PrintLog("Remove all tables", null, error);
	}.bind(this), function() {
		PrintLog("Remove all tables", null, "success");
	}.bind(this));
};

//On logout, drop all the tables
SQLiteOps.prototype.DropTable = function(tableName) {
	var query = "Drop table " + tableName;
	this.db.transaction(function(tx) {
		tx.executeSql(query, [], function(tx, res) {
			PrintLog(res);
		}, function(tx, error) {
			PrintLog("Drop Table", tableName, error);
		});
	}, function(error) {
		PrintLog("Drop Table", tableName, error);
	}, function() {
		PrintLog("Drop Table", tableName, "success");
	});
};
SQLiteOps.prototype.ReadFromSQLite = function(tableName, itemId, callback) {
	var query = "Select data from " + tableName + " where item_id = ?";
	this.db.transaction(function(tx) {
		tx.executeSql(query, [itemId], function(tx, resultSet) {
			if(resultSet.rows.length > 0){
				callback(true, JSON.parse(resultSet.rows.item(0).data));
			}
			else{
				callback(false);
			}
		}, function(tx, error) {
			PrintLog("Read from SQLite", tableName, error.message);
			callback(false);
			return false;
		});
	}, function(error) {
		PrintLog("Read from SQLite", tableName, error);
		callback(false);
	}, function() {
		PrintLog("Select Data", tableName, "success");
	});
};

SQLiteOps.prototype.InsertItem = function(query, tableName, itemId, data, cb) {
	this.db.transaction(function(tx) {
		tx.executeSql(query, [itemId, data], function(tx, res) {
			PrintLog("Insert Data", tableName, "insert id: "+res.insertId);
			cb();
		}, function(tx, error) {
			PrintLog("Insert Data", tableName, error.message);
			cb();
			return false;
		});
	}, function(error) {
		PrintLog("Insert Data", tableName, error.message);
		cb();
	}, function() {
		PrintLog("Insert Data", tableName, "success");
	});
};

SQLiteOps.prototype.UpdateItem = function(query, tableName, itemId, data, cb) {
	this.db.transaction(function(tx) {
		tx.executeSql(query, [data, itemId], function(tx, res) {
			PrintLog("Update Data", tableName, "insert id: "+res.insertId);
			cb();
		}, function(tx, error) {
			PrintLog("Update Data", tableName, error.message);
			cb();
			return false;
		});
	}, function(error) {
		PrintLog("Update Data", tableName, error.message);
		cb();
	}, function() {
		PrintLog("Update Data", tableName, "success");
	});
};

SQLiteOps.prototype.CreateCahedDateTable = function(tableName, callback) {
	if(this.db == null)
	{
		callback(false);
		return;
	}
	var query = "Create table if not exists "+tableName+" (id INTEGER PRIMARY KEY AUTOINCREMENT, "+
			"route TEXT, lastCachedDate TEXT)";
	var num = Math.random();
	this.db.transaction(function(tx) {
		tx.executeSql(query);
	}, function(error) {
		PrintLog("Table creation", tableName, error.message);
		callback(false);
	}, function() {
		PrintLog("Table creation", tableName, "Successfully created table");
		/*
		this.db.transaction(function(tx) {
					tx.executeSql("CREATE INDEX "+tableName+"_id ON "+tableName+" (id);");
				}, function(error) {
					PrintLog("Index creation", tableName, error.message);
					callback(false);
				}, function() {
					PrintLog("Index creation", tableName, "Successfully created Index");
					callback(true);
				});*/
		
		callback(true);
	});
};
SQLiteOps.prototype.GetLastCachedDate = function(tableName, routeValue, callback) {
	if(this.db == null)
	{
		callback(0);
		return;
	}
	PrintLog("Inside Get last cached date");
	var query = "Select lastCachedDate from " + tableName + " where route = ?";
	var lastCachedDate = "";
	var len = 0;
	this.db.transaction(function(tx) {
		tx.executeSql(query, [routeValue], function(tx, resultSet) {
			lastCachedDate = resultSet.rows.item(0);
			len = resultSet.rows.length;
			callback(resultSet.rows.length, resultSet.rows.item(0));
		}, function(tx, error) {
			PrintLog("Get last cached date", tableName, error.message);
			callback(0);
		});
	}, function(error) {
		PrintLog("Get last cached date", tableName, error.message);
		callback(0);
	}, function() {
		PrintLog("Get last cached date", tableName, "success");
		//callback(len, lastCachedDate);
	});
}
SQLiteOps.prototype.InsertCachedDate = function(query, tableName, route, lastCachedDate) {
	this.db.transaction(function(tx) {
		tx.executeSql(query, [route, lastCachedDate], function(tx, res) {
			PrintLog("Insert cached date", tableName, "insert id: "+res.insertId);
			PrintLog("Insert cached date", tableName, "rows affected: "+res.rowsAffected);
		}, function(tx, error) {
			PrintLog("Insert cached date", tableName, error.message);
		});
	}, function(error) {
		PrintLog("Insert cached date", tableName, error.message);
	}, function() {
		PrintLog("Insert cached date", tableName, "success");
	});
};

SQLiteOps.prototype.UpdateCachedDate = function(query, tableName, route, lastCachedDate) {
	this.db.transaction(function(tx) {
		tx.executeSql(query, [lastCachedDate, route], function(tx, res) {
			PrintLog("Update cached date", tableName, "insert id: "+res.insertId);
			PrintLog("Update cached date", tableName, "rows affected: "+res.rowsAffected);
		}, function(tx, error) {
			PrintLog("Update cached date", tableName, error.message);
		});
	}, function(error) {
		PrintLog("Update cached date", tableName, error.message);
	}, function() {
		PrintLog("Update cached date", tableName, "success");
	});
};

SQLiteOps.prototype.WriteCachedDateToSQLite = function(route, lastCachedDate) {
	var query = "";
	var tableName = "routeDateMapping";
	this.CreateCahedDateTable(tableName, function(result) {
		if (result) {
			//Success
			
			this.GetLastCachedDate(tableName, route, function(records, value) {
				if (records > 0) {
					//record already exists, update
					query = "Update " + tableName + " SET lastCachedDate = ? where route = ?";
					this.UpdateCachedDate(query, tableName, route, lastCachedDate);
				} else {
					//insert a new record
					query = "INSERT INTO " + tableName + " (route, lastCachedDate) VALUES (?,?)";
					this.InsertCachedDate(query, tableName, route, lastCachedDate);
				}
			}.bind(this));
		} else {
			//Error in creating table
			PrintLog("Write cached date to SQLite", tableName, "Error creating table");
		}
	}.bind(this));
};


/* Image base64 encoding storage */
SQLiteOps.prototype.CreateImageBase64 = function(tableName, callback) {
	if(this.db == null)
	{
		callback(false);
		return;
	}
	
	var query = "Create table if not exists " + tableName + " (id INTEGER PRIMARY KEY AUTOINCREMENT, onlineUrl TEXT, encodedUrl BLOB)";
	this.db.transaction(function(tx) {
		tx.executeSql(query);
	}, function(error) {
		PrintLog("Table creation", tableName, error.message);
		callback(false);
	}, function() {
		PrintLog("Table creation", tableName, "Successfully created table");
		/*
		 this.db.transaction(function(tx) {
		 tx.executeSql("CREATE INDEX "+tableName+"_id ON "+tableName+" (id);");
		 }, function(error) {
		 PrintLog("Index creation", tableName, error.message);
		 callback(false);
		 }, function() {
		 PrintLog("Index creation", tableName, "Successfully created Index");
		 callback(true);
		 });*/
			callback(true);
	});
};
SQLiteOps.prototype.GetImageBase64 = function(tableName, onlineUrl, callback) {
	PrintLog("Inside Get Image Base 64");
	var query = "Select encodedUrl from ImageEncodingMapping where onlineUrl = ?";
	var encodedUrl;
	var len = 0;
	this.db.transaction(function(tx) {
		tx.executeSql(query, [onlineUrl], function(tx, resultSet) {
			encodedUrl = resultSet.rows.item(0);
			len = resultSet.rows.length;
			callback(resultSet.rows.length, resultSet.rows.item(0));
		}, function(tx, error) {
			PrintLog("Get Image Base 64", tableName, error.message);
			callback(0);
			return false;
		});
	}, function(error) {
		PrintLog("Get Image Base 64", tableName, error.message);
		callback(0);
	}, function() {
		PrintLog("Get Image Base 64", tableName, "success");
		//callback(len, lastCachedDate);
	});
};

SQLiteOps.prototype.InsertImageBase64 = function(query, tableName, onlineUrl, encodedUrl) {
	console.log("onlineUrl: "+onlineUrl);
	this.db.transaction(function(tx) {
		tx.executeSql(query, [onlineUrl, encodedUrl], function(tx, res) {
			PrintLog("Insert Image base 64", tableName, "insert id: " + res.insertId);
		}, function(tx, error) {
			PrintLog("Insert Image base 64", tableName, error.message);
		});
	}, function(error) {
		PrintLog("Insert Image base 64", tableName, error.message);
	}, function() {
	});
};
SQLiteOps.prototype.UpdateImageBase64 = function(query, tableName, onlineUrl, encodedUrl) {
	this.db.transaction(function(tx) {
		tx.executeSql(query, [encodedUrl, onlineUrl], function(tx, res) {
		PrintLog("Update Image base 64", tableName, "insert id: " + res.insertId);
			PrintLog("Update Image base 64", tableName, "rows affected: " + res.rowsAffected);
		}, function(tx, error) {
			PrintLog("Update Image base 64", tableName, error.message);
		});
	}, function(error) {
		PrintLog("Update Image base 64", tableName, error.message);
	}, function() {
	});
};
SQLiteOps.prototype.WriteImageBase64 = function(onlineUrl, encodedUrl) {
	var query = "";
	var tableName = "ImageEncodingMapping";
	this.CreateImageBase64(tableName, function(result) {
		if (result) {
			//Success
			this.GetImageBase64(tableName, onlineUrl, function(records, value) {
				if (records > 0) {
					//record already exists, update
					query = "Update " + tableName + " SET encodedUrl = ? where onlineUrl = ?";
					this.UpdateImageBase64(query, tableName, onlineUrl, encodedUrl);
				} else {
					//insert a new record
					query = "INSERT INTO " + tableName + " (onlineUrl, encodedUrl) VALUES (?,?)";
					this.InsertImageBase64(query, tableName, onlineUrl, encodedUrl);
				}
			}.bind(this));
		} else {
			//Error in creating table
			PrintLog("Write Image Base64 to SQLite", tableName, "Error creating table");
		}
	}.bind(this));
};
SQLiteOps.prototype.WriteTable = function(tableName,key, value) {
	var query = "INSERT INTO " + tableName + " (key, value) VALUES (?,?)";;
	this.CreateSQLiteTable(tableName, function(result) {
		if (result){
			//Success
			this.GetRecord(tableName, key, function(records, data) {
				if (records > 0) {
					//record already exists, update
					query = "Update " + tableName + " SET value = ? where key = ?";
				}
				this.SaveRecord(query, tableName, key, value);
			}.bind(this));
		} else {
			//Error in creating table
			PrintLog("Write Table", tableName, "Error creating table");
		}
	}.bind(this));
};
SQLiteOps.prototype.CreateSQLiteTable = function(tableName,callback) {
	if(this.db == null)
	{
		callback(false);
		return;
	}
	var query = "Create table if not exists " + tableName + " (id INTEGER PRIMARY KEY AUTOINCREMENT, key TEXT, value TEXT)";
	this.db.transaction(function(tx) {
		tx.executeSql(query);
	}, function(error) {
		PrintLog("Table creation", tableName, error.message);
		callback(false);
	}, function() {
		PrintLog("Table creation", tableName, "Successfully created table");
		callback(true);
	});
};
SQLiteOps.prototype.GetRecord = function(tableName, key, callback) {
	var query = "Select value from "+tableName+" where key = ?";
	this.db.transaction(function(tx) {
		tx.executeSql(query, [key], function(tx, resultSet) {
			callback(resultSet.rows.length, resultSet.rows.item(0));
		}, function(tx, error) {
			PrintLog("Get data", tableName, error.message);
			callback(0);
		});
	}, function(error) {
		PrintLog("Get data", tableName, error.message);
		callback(0);
	}, function() {});
};
SQLiteOps.prototype.SaveRecord = function(query,tableName, key, value) {
	this.db.transaction(function(tx) {
		tx.executeSql(query, [key, value], function(tx, res) {
			PrintLog("Save Record", tableName, "rows affected: " + res.rowsAffected);
		}, function(tx, error) {
			PrintLog("Save Record", tableName, error.message);
		});
	}, function(error) {
		PrintLog("Save Record", tableName, error.message);
	}, function() {
		PrintLog("Save Record", tableName, "success");
	});
};
//Check if Data exists for offline mode
SQLiteOps.prototype.CheckIfDataExists = function(tableName, key, callback) {
	var query = 'Select * from '+tableName+' where item_id = ?';
	console.log(query);
	this.db.transaction(function(transaction) {
		transaction.executeSql(query, [key], function(tx, result) {
			console.log(result.rows.length);
			callback(result.rows.length);
		},
		function(error){
			console.log("error");
			callback(0);
		});
	});
};

SQLiteOps.prototype.GetAllRecords = function(tableName, callback) {
	var query = "Select data from " + tableName;
	
	this.db.transaction(function(tx) {
		tx.executeSql(query, [], function(tx, resultSet) {
			callback(resultSet.rows.length, resultSet.rows);
		}, function(tx, error) {
			PrintLog("Select Data", tableName, error);
			callback(0);
		});
	}, function(error) {
		PrintLog("Select Data", tableName, error);
	}, function() {
		PrintLog("Select Data", tableName, "success");
	});
};
