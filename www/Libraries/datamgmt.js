var DataMgmt = function(){};

DataMgmt.onError = function(e)
{
	alert(e.msg);
	console.log(e);
};
DataMgmt.onSuccess = function(response)
{
	console.log(response);
};

DataMgmt.GetData = function(route, data, offlineAllowed, successClb, errorClb)
{
	if(typeof data == 'undefined')
	{
		data = {};
	}
	if(typeof offlineAllowed == 'undefined')
	{
		offlineAllowed = false;
	}
	if(typeof errorClb == 'undefined')
	{
		errorClb = DataMgmt.onError;
	}
	
	if(typeof successClb == 'undefined')
	{
		errorClb = DataMgmt.onSuccess;
	}
	
	var actionSegments = route.split("&");
	var id = 0;
	var routeName = actionSegments[0];
	if(actionSegments.length > 1)
	{
		var pairs = actionSegments[1].split("=");
		if(pairs[0] == "id")
		{
			id = pairs[1];
		}
	}
	//For report & solution
	routeName += DataMgmt.ManageEntity(actionSegments);
	var manageId = DataMgmt.ManageId(actionSegments);
	if(manageId != 0){
		id = manageId;
	}
	var url = remoteServer+"/route?route="+route+"&response-type=2&fromapp=1&loadall=1";
	var entityName = "Entity_" + routeName.replace(/[^a-zA-Z0-9_]+/g, "_");
	var offlineMode = null;
	if(typeof offlineAllowed != 'undefined' && offlineAllowed == true){
		offlineMode = {
			tablename: entityName,
			filename: entityName+'_'+id+'.json',
			recordId: id,
		};
	}
	tr$.ajax({
		url: url,
		timeout: 20000,
		offlineMode: offlineMode,
		progress: true,
		successClb: function(response){
			console.log("Inside success callback");
			successClb(response.data);
		},
		errorClb: function(e){
			console.log('Inside error callback');
			errorClb(e);
		},
	});
};

DataMgmt.GetSavedData = function(entityName, id, successClb, errorClb)
{
	var entityName = "Entity_" + entityName.replace(/[^a-zA-Z0-9_]+/g, "_");
	if(typeof errorClb == 'undefined')
	{
		errorClb = DataMgmt.onError;
	}
	
	if(typeof successClb == 'undefined')
	{
		errorClb = DataMgmt.onSuccess;
	}
	
	var fileErrorHandling = false;
	loadDataFromSQLite(entityName, id, function(status, result) {
		if(status){
			//SQLite success
			successClb(result);
		}
		else{
			if(result != undefined && result.result == "Error in SQLite"){
				UrlMgmt.getObj().debugEnabled = true;
				//Problem with SQLite (fallback to file system)
				if(UrlMgmt.getObj().debugEnabled){
					loadDataFromFile(entityName+"_"+id+'.json', fileErrorHandling, function(result1) {
						if (result1 == null) {
							errorClb({'msg':'Data not found'});
						}
						else{
							successClb(result1);
						}
					});
				}
				else{
					errorClb({'msg':'Data not found'});
				}
			}
			else{
				errorClb({'msg':'Data not found'});
			}
		}
	});
};
DataMgmt.SaveData = function(entityName, id, data, successClb, errorClb)
{
	var entityName = "Entity_" + entityName.replace(/[^a-zA-Z0-9_]+/g, "_");
	if(typeof errorClb == 'undefined')
	{
		errorClb = DataMgmt.onError;
	}
	
	if(typeof successClb == 'undefined')
	{
		errorClb = DataMgmt.onSuccess;
	}
	if (typeof window.sqlitePlugin !== 'undefined' && SQLiteOps.getObj().databaseOpen == true) {
		var sqliteObj = SQLiteOps.getObj();
		sqliteObj.WriteToSQLite(entityName, id, JSON.stringify(data), function(){
			if ( typeof successClb !== "undefined")
				successClb();
				return false;	
		});
	}
	else{
		UrlMgmt.getObj().debugEnabled = true;
		//Storing in file as a fallback
		if (UrlMgmt.getObj().debugEnabled) {
			writeToFile(entityName+"_"+id+'.json', data, false, function() {
				if ( typeof successClb !== "undefined")
					successClb();
			});
		}
		else {
			if(typeof errorClb !== "undefined")
				errorClb();
		}
	}
};
DataMgmt.ManageEntity = function(actionSegments)
{
	if(actionSegments.length > 0){
		for(var i = 0;i < actionSegments.length ;i++){
			var str = actionSegments[i].split('=');
			if(str[0] == 'report'){
				return 'report';
			}
			else if(str[0] == 'solution'){
				return 'solution';
			}
		}
	}
	return '';
};
DataMgmt.ManageId = function(actionSegments)
{
	if(actionSegments.length > 0){
		for(var i = 0;i < actionSegments.length ;i++){
			var str = actionSegments[i].split('=');
			if(str[0] == 'sessionid' || str[0] == 'session'){
				return str[1];
			}
		}
	}
	return 0;
};