/******* TR Helper *******/
var TRHelper = function() {
};
var tr$ = new TRHelper();
tr$.appUpdateInProgress = 0;

TRHelper._version = 3.1;
TRHelper.onError = function(e)
{
	alert(e.msg);
	console.log(e);
};
TRHelper.onSuccess = function(response)
{
	console.log(response);
};
TRHelper.LoadCSS = function(FileName) {
	var id = "CSS_" + FileName.replace(/[^a-zA-Z0-9_]+/, "_").replace(".", "_");
	if (!document.getElementById(id)) {
		link = document.createElement('link');
		link.id = id;
		link.type = 'text/css';
		link.rel = 'stylesheet';
		link.href = FileName + "?i=" + TRHelper._version;
		document.head.appendChild(link);
	}
};

TRHelper.LoadJS = function(src, callback) {
	var id = "JS_" + src.replace(/[^a-zA-Z0-9_]+/, "_").replace(".", "_");
	if (!document.getElementById(id)) {
		var s = document.createElement('script');
		s.id = id;
		s.src = src + "?i=" + TRHelper._version;
		s.async = true;
		s.onreadystatechange = s.onload = function() {
			if ( typeof this.callback != 'undefined') {
				this.callback();
			}
		}.bind({
			obj : s,
			callback : callback
		});
		document.getElementsByTagName('head')[0].appendChild(s);
	} else {
		if ( typeof callback !== "undefined") {
			callback.done = true;
			callback();
		}
	}
};

TRHelper.prototype.ajax = function(ajaxObj, obj) {
	var data = {};
	ajaxObj.url = ajaxObj.hasOwnProperty('url') ? ajaxObj.url : remoteServer + "/route?route=common/ajax";
	ajaxObj.timeout = ajaxObj.hasOwnProperty('timeout') ? ajaxObj.timeout : 20000;
	data.mod = ajaxObj.hasOwnProperty('mod') ? ajaxObj.mod : null;
	data.ack = ajaxObj.hasOwnProperty('ack') ? ajaxObj.ack : null;
	ajaxObj.progress = ajaxObj.hasOwnProperty('progress') ? ajaxObj.progress : false;
	if(data.mod != null && data.ack != null)
		ajaxObj.url += '&mod='+data.mod +'&ack='+data.ack;
	data.fromapp = 1;
	ajaxObj.additionalParams = ajaxObj.hasOwnProperty('additionalParams') ? ajaxObj.additionalParams : {};
	ajaxObj.offlineMode = ajaxObj.hasOwnProperty('offlineMode') ? ajaxObj.offlineMode : null;
	if (ajaxObj.offlineMode != null) {
		if (ajaxObj.offlineMode.read == undefined) {
			ajaxObj.offlineMode.read = true;
		}
		if (ajaxObj.offlineMode.write == undefined) {
			ajaxObj.offlineMode.write = true;
		}
	}
	for(var param in ajaxObj.additionalParams){
		data[param] = ajaxObj.additionalParams[param];
	}
	ajaxObj.url += '&clientVersion='+TRHelper._version;
	ajaxObj.successClb = ajaxObj.hasOwnProperty('successClb') ? ajaxObj.successClb : TRHelper.onSuccess;
	ajaxObj.errorClb = ajaxObj.hasOwnProperty('errorClb') ? ajaxObj.errorClb : TRHelper.onError;
	
	if (!UrlMgmt.getObj().App.isOnline) {
		if (ajaxObj.offlineMode != null && ajaxObj.offlineMode.read == true) {
			loadExistingData({entity: ajaxObj.offlineMode.tablename, recordId: ajaxObj.offlineMode.recordId}, function(reply){
				if(typeof ajaxObj.successClb == 'function' )
				{
					ajaxObj.successClb(reply, obj);
					return false;
				}
			}, function(e){
				ajaxObj.errorClb({'msg':'Must be online'}, obj);
			});
		} else {
			ajaxObj.errorClb({'msg':'Must be online'}, obj);
		}
	} else {
		$.ajax({
			url : ajaxObj.url,
			type : 'POST',
			data : data,
			xhrFields : {
				withCredentials : true
			},
			crossDomain : true,
			context : this,
			timeout : ajaxObj.timeout,
			
			
			xhr: function() {  
							var myXhr = $.ajaxSettings.xhr();
							if(myXhr.upload){ 
								myXhr.upload.addEventListener('progress', function(e){
									var percent = e.loaded * 100 / e.total;
								}.bind(this), false); 
							}
							myXhr.addEventListener('progress', function(e){
								var percent = e.loaded * 100 / e.total;
								$('.Loader').attr('data-pct', parseInt(percent));
								if(ajaxObj.progress == true){
									//11.63 is the circumference
									var val = (11.63/100)*percent;
									$('.Loader .progress-loader path').attr("stroke-dasharray", val+", 100");
								}
								else{
									$('.Loader .progress-loader path').attr("stroke-dasharray", "11.63, 100");
								}
							}.bind(this), false);
							return myXhr;
						}.bind(this),
			
			
			
			success : function(reply) {
				var latestVersionObj = {};
				if(typeof reply.version != 'undefined'){
					latestVersionObj = reply.version;
					UrlMgmt.getObj().App.version = reply.version;
				}
				else if(typeof reply.result != 'undefined' && typeof reply.result.version != 'undefined'){
					latestVersionObj = reply.result.version; //version.php
					UrlMgmt.getObj().App.version = reply.result.version;
				}
				if(typeof latestVersionObj != 'undefined'){
					tr$.AppUpdate(latestVersionObj);
				}
				ajaxObj.successClb(reply, obj);
				if (ajaxObj.offlineMode != null && ajaxObj.offlineMode.write == true){// && reply.result.state == "ok") {
					writeData(ajaxObj.offlineMode.filename, ajaxObj.offlineMode.tablename, ajaxObj.offlineMode.recordId, reply, function() {});
				}
			}.bind(obj),
			error : function(jqXHR, exception) {
				if (jqXHR.status === 0) {
					printHelperLog("Ajax call", "Offline Mode");
					if (ajaxObj.offlineMode != null && ajaxObj.offlineMode.read == true) {
						loadExistingData({entity: ajaxObj.offlineMode.tablename, recordId: ajaxObj.offlineMode.recordId}, function(reply){
							if(typeof ajaxObj.successClb == 'function' )
							{
								ajaxObj.successClb(reply, obj);
								return false;
							}
						}, function(e){
							ajaxObj.errorClb({'msg':'Must be online'}, obj);
						});
					}
					else {
						ajaxObj.errorClb({'msg':'Must be online'}, obj);
					}
				} else if (jqXHR.status == 404) {
					alert('Requested page not found. [404]');
				} else if (jqXHR.status == 500) {
					alert('Internal Server Error [500].');
				} else if (exception === 'parsererror') {
					alert('Requested JSON parse failed.');
				} else if (exception === 'timeout') {
					if (ajaxObj.offlineMode != null && ajaxObj.offlineMode.read == true) {
						loadExistingData({entity: ajaxObj.offlineMode.tablename, recordId: ajaxObj.offlineMode.recordId}, function(reply){
							if(typeof ajaxObj.successClb == 'function' )
							{
								ajaxObj.successClb(reply, obj);
								return false;
							}
						}, function(e){
							ajaxObj.errorClb({'msg':'Must be online'}, obj);
						});
					}
					else {
						ajaxObj.errorClb({'msg':'Must be online'}, obj);
					}
				} else if (exception === 'abort') {
					alert('Ajax request aborted.');
				} else {
					alert('Uncaught Error.\n' + jqXHR.responseText);
				}
			}.bind(obj)
		});
	}
};

/* Version check for app starts */
TRHelper.prototype.AppUpdate = function(latestVersionObj,updateFlag){
	UrlMgmt.getObj().notifyUpdate = false;
	//return if app update is in progress
	if(tr$.appUpdateInProgress) return;
	
	if(typeof tr$.AppUpdateProgress !== 'function')
	{
		tr$.AppUpdateProgress = function(progressPercent){
			if(typeof this.progressPercent === 'undefined')
			{
				this.progressPercent = 0;
			}
			if(typeof progressPercent !== 'undefined' && this.progressPercent < progressPercent)
			{
				this.progressPercent = progressPercent;
				$('#app-updater .Loader .Percentage span').css("width", this.progressPercent+"%");
				console.log(this.progressPercent);
			}
			if(this.progressPercent >= 70)
			{
				clearInterval(this.progressInterval);
			}
		};
	}

	if(TRHelper._version < parseFloat(latestVersionObj['id']))
	{ 
		//hard update
		if(latestVersionObj['type'] == 0){
			$('.versioncontent').text("App Current version is ["+TRHelper._version+"]. New version of the App ["+latestVersionObj['id'] +"] has been Released you have to update it.");
			tr$.appUpdateInProgress =1;
		 	$('.VersionCheck').show();
			$('.softupdate').hide();
			 
		 	$('.appupdate').unbind().bind("click",function(){
				//open play store
				cordova.plugins.market.open('com.TopRankers.trapp');
		 	});
		} 
		//soft update
		else if(latestVersionObj['type'] == 1){
			$('.versioncontent').text("App Current version is ["+TRHelper._version+"]. New version of the App ["+latestVersionObj['id'] +"] has been Released you have to update it.");
			tr$.appUpdateInProgress =1;
			var options = {
				'auto-download' : false,
			 	'auto-install' : false
			};
			 
			$('.VersionCheck').show();
			$('.versionlink').hide();
		 	$('.appupdate').unbind().bind("click",function(){
		 	 
		 	 
		 	$('.VersionCheck').hide();
		 	$('#app-updater').show();
		 	$('#app-updater .Loader .alert').text("Downloading new updates!");
		 	$("#menu-bar").attr("showMenu", false);
		 	 
		 	//hcp update
		 	chcp.configure(options, HCP_configureCallback);
		 	setTimeout(function(){ 
		 		$('#app-updater .Loader .alert').hide(); 
		 		$('#app-updater .Loader .Percentage').hide();
		 		$('#app-updater .Loader img').hide();
		 		$('#app-updater .Loader').append("<img src=\"img/dangerping.png\" style=\"/*! display: none; */width: 50px;height: 50px;top: 39px;\"><div class=\"alert alert-info errorMsg\" style=\"margin-top:32px;\">Downloading new updates Failed update it from playstore!</div><button type=\"button\" class=\"btn btn-primary\" onclick=\"cordova.plugins.market.open('com.TopRankers.trapp');\">Update</button>");
		 	}, 240000); 
		 	 
		 	tr$.progressInterval = setInterval(function() {
				if(typeof tr$.progressPercent === 'undefined')
				{
					tr$.progressPercent = 0;
				}
			 	tr$.AppUpdateProgress(tr$.progressPercent+10);
			 	},3000);
		 	});
		 }
		 //notify update
		 else if(latestVersionObj['type'] == 2){
		 	UrlMgmt.getObj().notifyUpdate = true; 
		 }
	 }
	 else if(typeof updateFlag !== 'undefined' && updateFlag == true)
	 {
	 	UrlMgmt.getObj().notifyUpdate = false;
	 	tr$.alert("Your app is latest version!");
	 }
};
/* Version check for app ends */

/* Helper functions starts */
//get length of an array or a object (for checking during data population)
TRHelper.prototype.length = function(obj) {
	if ( typeof obj === 'object' && obj != null) {
		return Object.keys(obj).length;
	}
	return 0;
};
//get date followed by suffix (1st, 2nd, 3rd, ...) and abbreviated month name (Jan to Dec)
TRHelper.prototype.dateFormat = function(inputDate) {
	var dateObj = new Date(inputDate);
	if(dateObj == 'Invalid Date'){
		return inputDate;
	}
	date = dateObj.getDate();
	month = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(",")[dateObj.getMonth()];
	suffix = '';
	if (date > 3 && date < 21) {
		suffix = "th";
	} else {
		switch (date % 10) {
		case 1:
			suffix = "st";
			break;
		case 2:
			suffix = "nd";
			break;
		case 3:
			suffix = "rd";
			break;
		default:
			suffix = "th";
		}
	}
	return date + suffix + ' ' + month;
};
//get date followed by suffix (1st, 2nd, 3rd, ...) and abbreviated month name (Jan to Dec)
//get time in 12 hour AM/PM format
TRHelper.prototype.dateAndTimeFormat = function(inputDate) {
	var formattedDate = this.dateFormat(inputDate);
		dateObj = new Date(inputDate);
		hours = dateObj.getHours();
		minutes = dateObj.getMinutes();
		ampm = hours >= 12 ? 'PM' : 'AM';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0'+minutes : minutes;
	var formatedTime = hours + ':' + minutes + ' ' + ampm;
	return formattedDate+' '+formatedTime;
};
//get date followed by suffix (1st, 2nd, 3rd, ...) and abbreviated month name (Jan to Dec) with full year
TRHelper.prototype.dateFormatWithYear = function(inputDate) {
	var formattedDate = this.dateFormat(inputDate);
	dateObj = new Date(inputDate);
	return  formattedDate+' '+dateObj.getFullYear();
};
/*
The functions Math.round() and .toFixed() is meant to round to the nearest integer.
You'll get incorrect results when dealing with decimals and using the "multiply and 
divide" method for Math.round() or parameter for .toFixed(). For example, if you try 
to round 1.005 using Math.round(1.005 * 100) / 100 then you'll get the result of 1, 
and 1.00 using .toFixed(2) instead of getting the correct answer of 1.01.
You can use following to solve this issue:
 */
TRHelper.prototype.round = function(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
};
/*Replace spaces with dashes and make all letters lower-case*/
TRHelper.prototype.strReplace = function(string) {
    return typeof string === 'string' ? (string.replace(/[^a-zA-Z ]/g, "")).replace(/\s+/g, '-').toLowerCase():'';
};
/* Halper functions ends */

/* Functions to read/write the data from/to SQLite/file */
function loadExistingData(entityObj, successClb, errorClb) {
	var tablename = entityObj.hasOwnProperty('entity') ? entityObj.entity : "";
	var recordId = entityObj.hasOwnProperty('recordId') ? entityObj.recordId : 1;
	var filename = "";
	if(recordId == "1"){
		filename = tablename+".json";
	}
	else{
		filename = tablename +"_"+ recordId +".json";
	}
	var fileErrorHandling = entityObj.hasOwnProperty('fileErrorHandling') ? entityObj.fileErrorHandling : true;
	
	loadDataFromSQLite(tablename, recordId, function(status, result) {
		if(status){
			//SQLite success
			if(typeof successClb != 'undefined')
				successClb(result);
		}
		else{
			if(result != undefined && result.result == "Error in SQLite"){
            	UrlMgmt.getObj().debugEnabled = true;
				//Problem with SQLite (fallback to file system)
				if(UrlMgmt.getObj().debugEnabled){
					loadDataFromFile(filename, fileErrorHandling, function(result1) {
						if (result1 == null) {
							if(typeof errorClb != 'undefined')
								errorClb({'msg':'Data not found'});
						}
						else{
							if(typeof successClb != 'undefined')
								successClb(result1);
						}
					});
				}
				else{
					if(typeof errorClb != 'undefined')
						errorClb({'msg':'Data not found'});
				}
			}
			else{
				if(typeof errorClb != 'undefined')
					errorClb({'msg':'Data not found'});
			}
		}
	});
}
function loadDataFromSQLite(tablename, recordId, cb) {
	printHelperLog("Loading Existing Data (SQLITE)", "tablename: " + tablename + ", recordId: " + recordId);
	if (typeof window.sqlitePlugin == 'undefined' || SQLiteOps.getObj().databaseOpen == false) {
		cb(false, {"result":"Error in SQLite"});
	}
	else{
		SQLiteOps.getObj().ReadFromSQLite(tablename, recordId, function(status, result) {
			cb(status, result);
		});
	}
}
function loadDataFromFile(filename, fileErrorHandling, cb) {
	printHelperLog("Loading Existing Data (File)", "filename: " + filename);
	readFromFile(filename, function(result) {
		console.log(result);
		cb(result);
	}, function(fileName, e) {
		if(fileErrorHandling){
			switch (e.code) {
			case FileError.QUOTA_EXCEEDED_ERR:
				alert('Storage quota exceeded');
				break;
			case FileError.SECURITY_ERR:
				alert('Security error');
				break;
			case FileError.NOT_FOUND_ERR:
			case FileError.INVALID_MODIFICATION_ERR:
			case FileError.INVALID_STATE_ERR:
				alert("Could not display data. Please connect to internet.");
				break;
			default:
				alert('Unknown error');
				break;
			}
		}
		cb(null);
	});
}
function writeData(filename, tablename, recordId, data, successClb, errorClb) {
	if (typeof window.sqlitePlugin != 'undefined' && SQLiteOps.getObj().databaseOpen == true) {
		var sqliteObj = SQLiteOps.getObj();
		sqliteObj.WriteToSQLite(tablename, recordId, JSON.stringify(data), function(){
			if ( typeof successClb !== "undefined"){
				successClb();
				return false;
			}
		});
	}
	else{
		UrlMgmt.getObj().debugEnabled = true;
		if (UrlMgmt.getObj().debugEnabled) {
			writeToFile(filename, data, fileErrorHandler, function() {
				if ( typeof successClb !== "undefined")
					successClb();
			}.bind(this));
		}
		else {
			if(typeof errorClb !== "undefined")
				errorClb();
		}
	}
}

function checkIfDataExists(tablename, recordId, callback, errcb) {
	if (typeof window.sqlitePlugin != 'undefined') {
		var sqliteObj = SQLiteOps.getObj();
		if(sqliteObj.databaseOpen == true){
			sqliteObj.ReadFromSQLite(tablename, recordId, function(status, result) {
				console.log(result);
				if (status == 'false') {
					callback(0, recordId);
				} else {
					callback(1, recordId);
				}
			});
		}
		else{
			loadDataFromFile(tablename+"_"+recordId+".json", false, function(result){
				if(result == null){
					callback(0, recordId);
				}
				else{
					callback(1, recordId);
				}
			});
		}
	}
	else{
		loadDataFromFile(tablename+"_"+recordId, false, function(result){
			if(result == null){
				callback(0, recordId);
			}
			else{
				callback(1, recordId);
			}
		});
	}
}
/* Functions to read/write the data from/to SQLite/file end */

//TODO: Common logger for app
function printHelperLog(action, message) {
	console.log("HELPER:" + new Date().toLocaleString() + ": Action:" + action + ", Message:" + message);
}

TRHelper.prototype.alert = function(request) {
	$("#alert-modal").find(".alertmsg").html(request);
	$("#alert-modal").modal("show");
};

TRHelper.prototype.touchScrollAnimation = function(view) {
	var flag = true;
	var interval = null;
	$(view).swipe({
		swipeUp : function(event, direction, distance, duration, fingerCount) {
			if(flag == true)
			{
				$('#menu-bar').addClass("dasams");
				interval = setTimeout(function(){
					$('#menu-bar').removeClass("dasams");
					flag = true;
				},3000);
				flag = false;
			}
		},
		swipeDown : function(event, direction, distance, duration, fingerCount) {
			if(flag == false)
			{
				clearTimeout(interval);
				$('#menu-bar').removeClass("dasams");
				flag = true;
			}
		},
		swipeLeft : function(event, direction, distance, duration, fingerCount) {
			//event.preventDefault();
		},
		swipeRight : function(event, direction, distance, duration, fingerCount) {
			//event.preventDefault();
		},
		allowPageScroll : "vertical",
		threshold : 10,
		fingerReleaseThreshold : 100,
		preventDefaultEvents: false,
	});

};

TRHelper.prototype.carouselSet = function(carouselDom, view) {
	//don't use auto-carousel
	swipediv = carouselDom[0];
	var swipestartx = 0;
	var swipestarty = 0;
	var touchflag = true;
	var status = null;
	swipediv.addEventListener("touchstart", function(e) {
		swipestartx = e.changedTouches[0].pageX;
		swipestarty = $(view).scrollTop();
	}, false);

	swipediv.addEventListener("touchmove", function(e) {
		var swipestopx = e.changedTouches[0].pageX;
		var swipestopy = $(view).scrollTop();
		if (touchflag == true) {
			if (swipestopx > (swipestartx + 5)) {
				carouselDom.carousel("prev");
			} else if (swipestopx < (swipestartx - 5)) {
				carouselDom.carousel("next");
			} else {
				//e.preventDefault();
			}
			touchflag = false;
		}
	}, false);

	swipediv.addEventListener("touchend", function(e) {
		touchflag = true;
	}, false);
};

TRHelper.prototype.menubarConflict = function(element, view) {

	//function for elements conflicting with menubar animation.
	//do not use this for carousel, it already has it.

	var swipediv = $(view).find(element);
	var swipestarty = 0;

	swipediv[0].addEventListener("touchstart", function(e) {
		swipestarty = $(view).scrollTop();
	}, false);

	swipediv[0].addEventListener("touchmove", function(e) {
		var swipestopy = $(view).scrollTop();
		if (swipestopy > swipestarty) {
			$('#menu-bar').attr("showMenu", false);
		} else if (swipestopy < swipestarty) {
			$('#menu-bar').attr("showMenu", true);
		} else {
			//nothing.
		}
	}, false);
};

TRHelper.prototype.reverseObjectIndex = function(object) {
	var keys = Object.keys(object);
	var revkey = keys.reverse();
	return revkey;
};

/* Functions for ImageBase64 encoding */
TRHelper.prototype.ImageBase64Encoding = function(src, callback) {
	var xhttp = new XMLHttpRequest();

	xhttp.onload = function() {
		var fileReader = new FileReader();
		fileReader.onloadend = function() {
			callback(fileReader.result, src);
		};
		fileReader.readAsDataURL(xhttp.response);
	};

	xhttp.responseType = 'blob';
	xhttp.open('GET', src, true);
	xhttp.send();
};
//Pass the view object to get all the images from
TRHelper.prototype.StoreImagesInBase64 = function(view) {
	const regex = /\b((https|http):\/\/(cdn|s3))[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i;
	
	var allImages = $(view).find('img');
	for (var i = 0; i < allImages.length; i++) {
		if (regex.test(allImages[i].src)) {
			this.ImageBase64Encoding(allImages[i].src, function(dataURL, srcURL) {
				if (!(typeof window.sqlitePlugin == 'undefined' || SQLiteOps.getObj().databaseOpen == false)) {
					SQLiteOps.getObj().WriteImageBase64(srcURL, dataURL);
				}
			}.bind(this));
		}
	}
};
TRHelper.prototype.StoreSolutionImagesInBase64 = function(view) {
	const regex = /\/attachment\//;
	var allImages = $(view).find('img');
	for (var i = 0; i < allImages.length; i++) {
		var imgUrl = $(allImages[i]).attr('src');
		if (regex.test(imgUrl)) {
			this.ImageBase64Encoding(imgUrl, function(dataURL, srcURL) {
				if (!(typeof window.sqlitePlugin == 'undefined' || SQLiteOps.getObj().databaseOpen == false)) {
					SQLiteOps.getObj().WriteImageBase64(srcURL, dataURL);
				}
			}.bind(this));
		}
	}
};
function getImageInOffline(imageUrl, i, callback) {
	if (!(typeof window.sqlitePlugin == 'undefined' || SQLiteOps.getObj().databaseOpen == false)) {
		SQLiteOps.getObj().GetImageBase64("ImageEncodingMapping", imageUrl, function(count, url) {
			if (count > 0) {
				callback(url["encodedUrl"], i);
			} else {
				callback("", i);
			}
		});
	}
	else{
		callback("", i);
	}
};
function populateOfflineImages(view) {
	for (var i = 0; i < $(view).find('img').length; i++) {
		getImageInOffline($(view).find('img')[i].src, i, function(imgSrc, index) {
			if (imgSrc != "")
				$(view).find('img')[index].src = imgSrc;
		});
	}
}
function populateOfflineSolutionImages(view) {
	var allImages = $(view).find('img');
	for (var i = 0; i < allImages.length; i++) {
		var imgUrl = $(allImages[i]).attr('src');
		getImageInOffline(imgUrl, i, function(imgSrc, index) {
			if (imgSrc != "")
				$(view).find('img')[index].src = imgSrc;
		});
	}
}
TRHelper.prototype.appendRemoteHost = function(view)
{
	$(view).find('img').each(function () {
		var src = $(this).attr('src');
		if(src.startsWith('data:image')){}
		else{
			$(this).attr('src',remoteServer+src);
		}
    });
};
/* Functions for ImageBase64 encoding end*/

function sortObject(obj)
{
	var sortedObj = [];
	for (var i in obj) {
	    sortedObj.push([obj[i], new Date(obj[i]["tr1Data"]["startDate"])]);
	}
	
	//Sort on startDate (descending order)
	sortedObj.sort(function(a, b) {
	    return b[1] - a[1];
	});
	return sortedObj;
}
function sortObjectOnListIndex(obj)
{
	//Sort on listIndex ascending order, then sort on id ascending order
	var sortedObj = [];
	for (var i in obj) {
		sortedObj.push([obj[i], obj[i]["listIndex"],obj[i]["id"]]);
	}
	sortedObj.sort(function(a, b) {
    	return a[1] - b[1] || a[2] - b[2];
	});
	var resultObj = [];
	for(var obj in sortedObj){
		resultObj.push(sortedObj[obj][0]);
	}
	return resultObj;
}
function getMatchingValue(obj, key, value){
	var array = obj.filter(function(o){
		return o[key] == value;
	});
	return array[0];
}
TRHelper.prototype.OpenAllLinksInAppBrowser = function(parentDom){
	parentDom.find("[href]").each(function() {
		var href = $(this).attr('href');
		$(this).click(this,function(e){
			e.preventDefault();
			AppBrowser.getObj().OpenWPBlog(href);
		});
	});
};
function guid() {
	var d = new Date();
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}

	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + d.getTime();
}

/* URL redirection */

function URLRedirection(url, urlaction)
{
	if(url.startsWith('#')){
		var urlMgmt = UrlMgmt.getObj();
		var action = urlMgmt.actionStack[urlMgmt.actionStack.length-1].controler;
		action.stableDom.animate({scrollTop: $(action.stableDom.find(url)).offset().top - 60}, 1000);
		return;
	}
	//Handle checkout redirection
	if(url.startsWith(remoteServer+'/checkout')){
		getAction(url.replace(remoteServer+'/checkout?', 'common/checkout&'), url, url);
		return;
	}
	//Handle blogs redirection
	if(url.startsWith(remoteServer+'/exams')){
		if(!UrlMgmt.getObj().App.isOnline){
			UrlMgmt.getObj().App.onOffline();
			return false;
		}
		AppBrowser.getObj().OpenWPBlog(url);
		return;
	}
	var str = url.split('#')[0];
	if (str.length > 0) {
		str = str.split('/');
		str = str.filter(function(value) {
			return value.length > 0;
		});
		var urlKey = str.pop();
		getAction(urlaction, urlKey, url);
	} else {
		if(!UrlMgmt.getObj().App.isOnline){
			UrlMgmt.getObj().App.onOffline();
			return false;
		}
		window.location = url;
	}
}
function getAction(urlaction, urlKey, href) {
	var flag = false;
	var urls = [];
	urls.push(href);
	var sqliteOp = SQLiteOps.getObj();
	var id = href.split('#')[1];
	var urlMgmt = UrlMgmt.getObj();
	if (typeof urlaction !== "undefined" && urlaction != "") {
		urlMgmt.Route({
			route : urlaction,
			needData : true,
			offlineAllowed: true,
			emptyStack: true
		}, function(){
			if(id != undefined){
				var action = urlMgmt.actionStack[urlMgmt.actionStack.length-1].controler;
				if(id.endsWith('Tests')){
					//Click the test tab
					action.stableDom.find('a[href^="#'+id+'"]').click();
					return;
				}
				action.stableDom.animate({scrollTop: $(action.stableDom.find('#'+id)).offset().top - 60}, 1000);
				return;
			}
		});
		//redirect directly
	} else if (urls.length > 0) {
		if (/current-affairs/i.test(href)) {
			urlMgmt.Route({
				route : 'app/currentaffairs',
				needData : false
			});
			return false;
		}
		tr$.ajax({
			mod : "tr2app",
			ack : "geturlaction",
			additionalParams : {
				urls : urls
			},
			successClb : function(response) {
				var data = response.data;
				if (Object.keys(data).length > 0) {
					for (var i in data) {
						if(data[i]["action"].startsWith('common/exam')){
							var examId = data[i]["action"].split('&')[1].split('=')[1];
							var testListId = urlMgmt.menuData.exams[examId]["testListId"][0];
							urlMgmt.Route({route: "lists/test&id="+testListId, needData: true, offlineAllowed: true});
						}
						else if(!data[i]["action"].startsWith('common/exam')){
							flag = true;
							urlMgmt.Route({
								route : data[i]["action"],
								needData : true,
								offlineAllowed: true,
							}, function(){
								if(id != undefined){
									var action = urlMgmt.actionStack[urlMgmt.actionStack.length-1].controler;
									if(id.endsWith('Tests')){
										//Click the test tab
										action.stableDom.find('a[href^="#'+id+'"]').click();
										return;
									}
									action.stableDom.animate({scrollTop: $(action.stableDom.find('#'+id)).offset().top - 60}, 1000);
									return;
								}
							});
							if (!( typeof window.sqlitePlugin == 'undefined' || SQLiteOps.getObj().databaseOpen == false)) {
								sqliteOp.WriteTable("urlaction", data[i].urlKey, data[i]["action"]);
							}
						}
					}
				}
				else if(!flag){
						var pat = /^https?:\/\//i;
						var urlparts = href.split('/');
						if (pat.test(href)) {
							urlparts = urlparts.slice(3);
						}
						urlparts = urlparts.filter(function(value) {
							return value.length > 0;
						});
						createTempSession("/" + urlparts.join('/'));
						//create temp session if redirecting to web
					}
			}.bind(this),
			errorClb : function() {
				sqliteOp.GetRecord("urlaction", urlKey, function(noOfrecords, data) {
					if (noOfrecords > 0) {
						urlMgmt.Route({
							route : data.value,
							needData: true,
							offlineAllowed: true,
						}, function(){
							if(id != undefined){
								var action = urlMgmt.actionStack[urlMgmt.actionStack.length-1].controler;
								action.stableDom.animate({scrollTop: $(action.stableDom.find('#'+id)).offset().top - 60}, 1000);
								return;
							}
						});
					} else {
						var pat = /^https?:\/\//i;
						var urlparts = href.split('/');
						if (pat.test(href)) {
							urlparts = urlparts.slice(3);
						}
						urlparts = urlparts.filter(function(value) {
							return value.length > 0;
						});
						createTempSession("/" + urlparts.join('/'));
						//create temp session if redirecting to web
					}
				});
			}
		});
	}
	else {
		alert("Connect to internet");
	}
}
function createTempSession(url) {
	tr$.ajax({
		mod : "tr2app",
		ack : "gettemplink",
		additionalParams : {
			url : url
		},
		successClb : function(response) {
			if (response.data != null)
				window.open(remoteServer + response.data, '_system');
			else
			{
				var url = '';
				if(this.url.includes('toprankers.com')){
					url = this.url;
				}
				else{
					url = remoteServer + this.url;
				}
				window.open(url, '_system');
			}
			$(".Loader").remove();
		}.bind({
			url : url
		}),
		errorClb : function() {
			alert("Connect to internet");
		}
	});
}

/* URL redirection ends */
function getTimeFormat(seconds){
	var secs = 0;
	var mins = 0;
	var hrs = 0;
	if(seconds >= 60){
		mins = parseInt(seconds / 60);
		secs = parseInt(seconds % 60);
		if(mins >= 60){
			hrs = parseInt(mins / 60);
			mins = parseInt(mins % 60);
		}
	}
	var result = (hrs > 0)?(hrs+" hrs "):"";
	return (result + mins+" mins");
}