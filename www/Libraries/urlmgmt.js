/******* Url Mgmt *******/
var Action = function(action)
{
	this.route = action.hasOwnProperty('route') ? action.route : null;
	this.filePath = this.route; 
	this.controllerName = this.route.replace("/", "")+"Controller"; 
	this.controller = null;
	this.data = action.hasOwnProperty('data') ? action.data : null;
	this.view = null;
};

Action.prototype.PrepareView = function()
{
	return this.LoadController();
};

Action.prototype.LoadController = function()
{
	var defer = $.Deferred();
	TRHelper.LoadJS("controller/"+this.filePath+".js", function() {
		this.controller = eval("new " + this.controllerName + "()");
		this.controller.data = this.data;
		defer.resolve();
	}.bind(this));
	return defer;
};

Action.prototype.LoadView = function(callback)
{	
	var viewFile = this.controller.view == "default"?"views/"+this.filePath+".html":this.controller.view;
	if(viewFile != "")
	{
		var cssFile = this.controller.style == "default"?"css/"+this.filePath+".css":this.controller.style;
		if(cssFile != "")
		{
			TRHelper.LoadCSS(cssFile);
		}
		this.view = $("<div id='" + this.controllerName + "' class='view controller-" + this.controllerName + " page transition' data-view-action = "+ this.route +" ></div>");
		this.view.load(viewFile, function() {
			this.callback();
		}.bind({obj:this, callback:callback}));
	}
};

var _urlMgmt = null;
var packageName = "com.lyricsapp.stutigaan";
var oneSignalAppId = "899f2838-2400-4360-94c5-be6ddd13c501";

var UrlMgmt = function(){
	this.actionStack = [];
	this.modalObj = null;
	
	this.menuAction = null;
	this.sideMenuAction = null;
	this.headerAction = null;
	
	this.showLoader = null;
	this.removeLoader = null;
	this.App = new Application();
};

UrlMgmt.getObj = function()
{
	if(_urlMgmt === null)
	{
		_urlMgmt = new UrlMgmt();
	}
	return _urlMgmt;
};
UrlMgmt.prototype.Route = function(route, callback)
{
	//UrlMgmt.getObj().Route({route: "folder/file"});
	//route is to tell which page to open, other than that everything is handled by page itself
	var action = new Action(route);
	var prepareView = action.PrepareView();
	//var loadData = action.LoadData();

	$.when(prepareView).done(function (jsloaded, data) {
		action.LoadView(function(){
			UrlMgmt.getObj().PushView(action, function(){
				typeof callback == 'function' && callback(action);
			});
		});
	}.bind(this));
};

//To refresh only the view (As on try again)
UrlMgmt.prototype.Refresh = function(route)
{
	var toRefresh = this.actionStack[this.actionStack.length-1];
	toRefresh.needData = toRefresh.hasOwnProperty('needData') ? toRefresh.needData : true;
	toRefresh.offlineAllowed = toRefresh.hasOwnProperty('offlineAllowed') ? toRefresh.offlineAllowed : false;
	if(toRefresh.needData)
	{
		var dataMgmt = DataMgmt.GetData(toRefresh.route, {}, toRefresh.offlineAllowed, function(response) {
			toRefresh.controller.data = response;
			if(typeof toRefresh.controller.terminate == 'undefined' || toRefresh.controller.terminate == false)
			{
				toRefresh.controller.RefreshView();
			}
			}.bind(this), function(e){
				console.log(e);
				toRefresh.controller.data = {'offline': true};
				alert('Connect to Internet');
			}.bind(this));
		}
		else
		{
			if(typeof toRefresh.controller.terminate == 'undefined' || toRefresh.controller.terminate == false)
			{
				toRefresh.controller.RefreshView();
			}
	}
};

UrlMgmt.prototype.PushView = function(action, callback)
{
	if(typeof this.emptyStack !== 'undefined' && this.emptyStack == true){
		//empty the actionStack except home
		for(var i = this.actionStack.length-1; i >=1 ; i--){
			this.actionStack[i].view.remove();
			this.actionStack.pop();
		}
	}
	if(this.actionStack.length > 0){
		$(this.actionStack[this.actionStack.length-1].view).addClass('left');
	}
	if(this.actionStack.length == 0)
	{
		$('#preloader').hide();
	}
	this.actionStack.push(action);
	if(action.view != null)
	{
		$("#app").append(action.view);
	}
	if(typeof action.controller.Init !== 'undefined')
	{
		action.controller.Init(action.view);
	}
	
	callback();
};
UrlMgmt.prototype.PopView = function(callback)
{
	if (this.actionStack.length > 0) {
		var action  = this.actionStack[this.actionStack.length - 1];
		this.actionStack.pop();
		if(action.view != null)
		{
			action.view.remove();
		}
	}
	//Refresh the previous view popview
	if(this.actionStack.length > 0){
		var action = this.actionStack[this.actionStack.length-1];
		$(action.view).removeClass('left');
		this.RefreshView(action, function(){
			if(typeof callback == 'function'){
				callback();
			}
		});
	}
	else{
		if(typeof callback == 'function'){
			callback();
		}
	}
};
/* Use this , if you just want to remove the view and
don't want to refresh the view irrespective of controller refresh value */
UrlMgmt.prototype.RemoveView = function(domId)
{
	var toRemove = -1;
	if(typeof domId != 'undefined'){
		if (this.actionStack.length > 0) {
			for(var i=0; i<this.actionStack.length; i++){
				if($(this.actionStack[i].controller.dom).attr('id') == domId){
					toRemove = i;
					break;
				}
			}
		}
	}
	else{
		toRemove = this.actionStack.length - 1;
	}
	if (this.actionStack.length > 0) {
		var action  = this.actionStack[toRemove];
		this.actionStack.splice(toRemove, 1);
		if(action.view != null)
		{
			action.view.remove();
		}
	}
};
UrlMgmt.prototype.ClearActionStack = function(full)
{
	if(typeof full == 'undefined'){
		full = true;
	}
	var till = 0;
	if(full == false){
		till = 1;
	}
	for(var i = this.actionStack.length-1; i >= till; i--){
		this.actionStack[i].view.remove();
		this.actionStack.pop();
	}
};
/*Refresh the entire view depending on the refresh member variable value
 * if refresh value is false, just refresh the layout
 */
UrlMgmt.prototype.RefreshView = function(action, callback)
{
	if(typeof action.controller.refresh != 'undefined' && action.controller.refresh == true){
		$(action.view).remove();
		this.actionStack.pop();
		var route = action.route.split('&')[0];
		if(typeof action.controller.clickMenuOnRefresh != 'undefined' && action.controller.clickMenuOnRefresh == true){
			this.menuAction.view.find('ul.menu-item').find('li[data-action="'+route+'"]').click();
			if(typeof callback == 'function'){
				callback();
				return false;
			}
		}
		var routeObj = {route: action.route, needData: action.needData, offlineAllowed: action.offlineAllowed};
		UrlMgmt.getObj().Route(routeObj, function(){
			if(typeof callback == 'function'){
				callback();
			}
		});
	}
	else{
		if(typeof action.controller.SetLayout !== 'undefined'){
			action.controller.SetLayout();
		}
		if(typeof callback == 'function'){
			callback();
		}
	}
};
UrlMgmt.prototype.PullToRefresh = function(id)
{
	//threshold value to trigger -> 100px
	var starty = 0;
	var stopy = 0;
	var scrollPos = 0;
	var appendFlag = true;
	var threshold = 100;
	var thresholdFlag = true;
	$('#'+id).bind("touchstart", function(e) {
		starty = e.originalEvent.touches[0].clientY;
		scrollPos = $('#'+id).scrollTop();
	});

	$('#'+id).bind("touchmove", function(e) {
		if(scrollPos == 0){
			stopy = e.originalEvent.changedTouches[0].clientY;
			if(stopy > starty && ((thresholdFlag && stopy - starty >=threshold) || !thresholdFlag)){
				thresholdFlag = false;
				//means scrolling down
				var diff = stopy - starty;
				if(appendFlag){
					
					var html = "<div class='refreshDiv' style='position:absolute;top:30px;width:20%;left:40%;height:30px;text-align:center;'><img src='img/refresh.svg' alt='refreshing' style='height:30px;width:30px;background-color:white;border-radius:50%;'/></div>";
					$('#app').append(html);
					appendFlag = false;
					
				}			
				if(diff <= 150 ){
					$('.refreshDiv').css('top', 40+diff-20+'px');
				}	
				console.log(diff);
			}
		}
	});

	$('#'+id).bind("touchend", function(e) {
		console.log('Touch end');
		//thresholdFlag = true;
		if(stopy > starty && !thresholdFlag){
			$('#app').find('.refreshDiv').css('top', '100px');
			thresholdFlag = true;
			UrlMgmt.getObj().Refresh();
			setTimeout(function(){
				//$('#'+id).css('top', '65px');
				$('#app').find('.refreshDiv').remove();
				appendFlag = true;
			}, 1000);
		}
	});
};
UrlMgmt.prototype.PopulateMenu = function(callback)
{
	/*if(this.menuAction == null)
	{
		this.menuAction = new Action({route:"common/menu", needData:false});
		this.menuAction.PrepareView().then(function(){
			this.menuAction.LoadView(function(){
				$("#app").append(this.menuAction.view);
				this.menuAction.view.hide();
				this.menuAction.controller.Init(this.menuAction.view);
			}.bind(this));
		}.bind(this));
	}*/
	if(this.sideMenuAction == null)
	{
		this.sideMenuAction = new Action({route:"common/sidemenu", needData:false});
		this.sideMenuAction.PrepareView().then(function(){
			this.sideMenuAction.LoadView(function(){
				$("#app").append(this.sideMenuAction.view);
				this.sideMenuAction.view.hide();
				this.sideMenuAction.controller.Init(this.sideMenuAction.view);
			}.bind(this));
		}.bind(this));
	}
	
	if(this.headerAction == null)
	{
		this.headerAction = new Action({route:"common/header", needData:false});
		this.headerAction.PrepareView().then(function(){
			this.headerAction.LoadView(function(){
				$("#app").append(this.headerAction.view);
				this.headerAction.view.hide();
				this.headerAction.controller.Init(this.headerAction.view);
			}.bind(this));
		}.bind(this));
	}
};
UrlMgmt.prototype.InitiateViewProperties = function()
{
	if(this.showLoader == null){
		this.showLoader = function(needData) {
			if($('.Loader.Freez').length == 0){
				var html = '<div class="Loader Freez" data-pct="0">'+
					'<svg style="position: absolute;top: 50%;transform: translate(-50%,-50%);" viewBox="0 0 36 36">'+
						  '<path '+
								'd="M18 16 '+
							  'a 2 2 0 0 1 0 4 '+
							  'a 2 2 0 0 1 0 -4" '+
							'fill="none" '+
							'stroke="#30B8E2"; '+
							'stroke-width="0.1"; '+
							'stroke-dasharray="100, 100"'+
						  '/>'+
					'</svg>'+
					'<svg class="progress-loader" style="position: absolute;top: 50%;transform: translate(-50%,-50%);" viewBox="0 0 36 36">'+
						  '<path '+
								'd="M18 16.15 '+
							  'a 1.85 1.85 0 0 1 0 3.7 '+
							  'a 1.85 1.85 0 0 1 0 -3.7" '+
							'fill="none" '+
							'stroke="#30B8E2"; '+
							'stroke-width="0.3"; '+
							'stroke-dasharray="0, 100"'+
						  '/>'+
					'</svg></div>';
				$('body').append(html);
				if(typeof needData != 'undefined' && needData == false){
					$('.Loader .progress-loader path').attr("stroke-dasharray", "11.63, 100");
					$('.Loader').attr('data-pct', '100');
				}
				setTimeout(this.removeLoader, 50000);
			}
		};
	}
	if(this.removeLoader == null){
		this.removeLoader = function(value) {
			if (typeof value === 'undefined') {
				value = 500;
			}
			if ($('.Loader.Freez').length > 0) {
				setTimeout(function() {
					$('.Loader.Freez').remove();
				}, value);
			}
			this.lazyload && this.lazyload.update();
		};
	}
	if(this.unlockRotation == null){
		this.unlockRotation = function() {
			screen.orientation.unlock();
		};
	}
	if(this.lockRotation == null){
		this.lockRotation = function() {
			screen.orientation.lock('portrait');
		};
	}
};
/************App**********/
var Application = function()
{
	$(document).bind('deviceready', function(){
		try {
			setTimeout(function(){
				$('.preloader').hide();
				urlMgmt.Route({route: "common/home"});
			}, 2000);
			
				var options = {
					'auto-download' : false,
					'auto-install' : false
				};
				$("#app").height(window.innerHeight-1).width(window.innerWidth);
				$("#app").height('100%');
				var urlMgmt = UrlMgmt.getObj();
				urlMgmt.InitiateViewProperties();
				urlMgmt.PopulateMenu();
				
				//One Signal notifications
				var notificationOpenedCallback = function(jsonData) {
					try{
						//By defaults, just opens the app
						var urlKey = jsonData.notification.payload.additionalData.url;
						var title = jsonData.notification.payload.additionalData.title;
						var filename = jsonData.notification.payload.additionalData.filename;
						
						var data = {title: "", filename: ""};
						var routeObj = {route: "", data: data};
						
						if(typeof urlKey != 'undefined' && urlKey.length > 0){
							routeObj.route = urlKey;
						}
						if(typeof title != 'undefined' && title.length > 0){
							data.title = title;
						}
						if(typeof filename != 'undefined' && title.filename > 0){
							data.filename = filename;
						}
						UrlMgmt.getObj().Route(routeObj);
						
						//TODO: Handle Playing the song, open the dialog - if songId is also passed in data
					}
					catch(e){
						console.log(e);
					}
				};
				if (typeof window.plugins.OneSignal != 'undefined') {
					window.plugins.OneSignal.startInit(oneSignalAppId)
											.inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
											.handleNotificationOpened(notificationOpenedCallback)
											.endInit();
				}
				
		} catch (e) {
			// Do nothing
			console.log(e);
		} finally {
		}
	});
	
	//onPluginsReady
	$(document).bind('online', this, function(e){
		//this.onOnline();
	}.bind(this));
	$(document).bind('offline', this, function(e){
		//e.data.isOnline = false;
		//this.onOffline();
		//NetworkStatusCheck.getObj().CheckStatus();
	}.bind(this));
	$(document).bind("resume", function(){
		
	});
	$(document).bind("backbutton", function(e){
		e.preventDefault();
		var urlMgmt = UrlMgmt.getObj();
		//Close the side menu, if opened
		urlMgmt.sideMenuAction.controller.CloseView();
		var actionStack = UrlMgmt.getObj().actionStack;
		if(urlMgmt.modalObj != null)
		{
			$('#song_dialog').find('span[data-dialog-close]').click();
			urlMgmt.modalObj = null;
			return false;
		}
		else if(actionStack.length == 1){
			if (window.confirm("Are you sure you want to exit ?")) {
				navigator.app.exitApp();
			}
			else{
				return false;
			}
		}
		else{
			urlMgmt.PopView();
		}
		return false;
	});
	$(document).bind("Pause", function(){
		
	});
	$(document).bind("resize", function(){
		
	});
};