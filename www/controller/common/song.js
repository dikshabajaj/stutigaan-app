var commonsongController = function() {
	this.view = "default";
	this.style = "default";
	this.dom = null;
	this.data = null;
};
commonsongController.prototype.Init = function(viewDom)
{
	
};
commonsongController.prototype.SetLayout = function()
{
	
};
commonsongController.prototype.OnlineInit = function() {
	this.LoadSong();
	this.BindEvents();
};
commonsongController.prototype.RefreshView = function()
{
	var urlMgmt = UrlMgmt.getObj();
	urlMgmt.showLoader();
	this.data.header = urlMgmt.menuData;
	this.OnlineInit();
	urlMgmt.removeLoader();
};
commonsongController.prototype.BindEvents = function()
{
};
