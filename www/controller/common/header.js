var commonheaderController = function(data)
{
	this.view = "default";
	this.style = "default";
	this.dom = null;
};
commonheaderController.prototype.Init = function(viewDom)
{
	this.dom = viewDom;
};
commonheaderController.prototype.DisplayHeader = function(pageView, title, sideMenu, backBtn, brandLogo)
{
	this.dom.show();
	this.dom.find('.page-head').find('.pageHeading').find('.pageHead-option').html('');
	this.dom.find('.page-head').find('.pageHeading').find('.pageHead-option').hide();
	var urlMgmt = UrlMgmt.getObj();
	this.dom.show();
	this.dom.find('.page-head').find('.pageHeading').find('span').html(title);
	$('#app').find('#touchablehighlight').remove();
	this.dom.find('.page-head').find('.header-back-btn').hide();
	this.dom.find('.page-head').find('.header-menu-btn').hide();
	if(sideMenu){
		pageView.append('<div id="touchablehighlight"></div>');
		urlMgmt.sideMenuAction.controller.forView = pageView;
		this.dom.find('.page-head').find('.header-menu-btn').unbind().bind('click', function(){
			console.log('Open side menu');
			urlMgmt.sideMenuAction.controller.OpenView();
		}.bind(this));
		this.dom.find('.page-head').find('.header-back-btn').hide();
		this.dom.find('.page-head').find('.header-menu-btn').show();
		this.TouchableHighlight();
	}
	if(backBtn){
		this.dom.find('.page-head').find('.header-back-btn').unbind().bind('click', function(){
			console.log('Clicked Back btn');
			UrlMgmt.getObj().PopView();
		}.bind(this));
		this.dom.find('.page-head').find('.header-menu-btn').hide();
		this.dom.find('.page-head').find('.header-back-btn').show();
	}
	if(!brandLogo){
		this.dom.find('.page-head').find('.pageHeading').find('.trbrand').hide();
	}
	else{
		this.dom.find('.page-head').find('.pageHeading').find('.trbrand').show();
	}
};
commonheaderController.prototype.ChangeHeaderTitle = function(title)
{
	this.dom.find('.page-head').find('.pageHeading').find('h4').html(title);
};
commonheaderController.prototype.AddHeaderOptions = function(options)
{
	this.dom.find('.page-head').find('.pageHeading').find('.pageHead-option').html(options);
	this.dom.find('.page-head').find('.pageHeading').find('.pageHead-option').show();
};
commonheaderController.prototype.SideMenuOpen = function()
{
	this.dom.find('.header-menu-btn').addClass('blocked');
};
commonheaderController.prototype.SideMenuClose = function()
{
	this.dom.find('.header-menu-btn').removeClass('blocked');
};
commonheaderController.prototype.TouchableHighlight = function()
{
	var headerHeight = this.dom.outerHeight(true);
		menuHeight = $('#menu-bar').outerHeight(true);
	$('#touchablehighlight').css('top',headerHeight);
	$('#touchablehighlight').width(screen.width/16).height((screen.height-headerHeight-menuHeight));
	
	/*$('#touchablehighlight').swipe({
   		swipeLeft: function(event, direction, distance, duration, fingerCount){
   			UrlMgmt.getObj().sideMenuAction.controller.CloseView();
   		}.bind(this),
   		swipeRight: function(event, direction, distance, duration, fingerCount){
   			UrlMgmt.getObj().sideMenuAction.controller.OpenView();
   		}.bind(this)
   	});*/
};
