var commonaboutusController = function() {
	this.view = "default";
	this.style = "default";
	this.dom = null;
	this.data = null;
};
commonaboutusController.prototype.Init = function(viewDom)
{
	this.dom = viewDom;
	this.SetLayout();
	this.BindEvents();
};
commonaboutusController.prototype.SetLayout = function()
{
	var urlMgmt = UrlMgmt.getObj();
	urlMgmt.headerAction.view.hide();
	//urlMgmt.headerAction.controller.DisplayHeader(this.dom, 'About Us', true, false, false);
};
commonaboutusController.prototype.BindEvents = function()
{
	$(this.dom).find('.header-back-btn').unbind().bind('click', function(){
		UrlMgmt.getObj().PopView();
	});
}