var commonmenuController = function()
{
	this.view = "default";
	this.style = "default";
	this.dom = null;
	this.ongoing_class_dom = null;
};
commonmenuController.prototype.Init = function(viewDom)
{
	this.dom = viewDom;
	this.RefreshMenuData(function(){
		this.PopulateMenu();
		this.BindEvents();
	}.bind(this));
};
commonmenuController.prototype.View = function()
{
	this.dom.show();
	var urlMgmt = UrlMgmt.getObj();
	var action = $(urlMgmt.actionStack[urlMgmt.actionStack.length-1].view).data('view-action');
	var actionData = urlMgmt.actionStack[urlMgmt.actionStack.length-1].controler.data;
	var listType = 0;
	if(typeof actionData != 'undefined' && typeof actionData.listType != 'undefined'){
		listType = actionData.listType;
		action = TR2ListType[listType];
	}
	var menuItem = $("#menu-bar").find("li[data-action='"+action+"']");
	if(menuItem.length == 0){
		 menuItem = $("#menu-bar").find("li[data-action='"+action.split('&')[0]+"']");
	}
	this.MenuItemActive(menuItem);
};
commonmenuController.prototype.BindEvents = function()
{
	$('.modal').on('shown.bs.modal', function(){
		urlMgmt.modalObj = $(this);
	});
	$('.modal').on('hidden.bs.modal', function(){
		urlMgmt.modalObj = null;
	});
};
commonmenuController.prototype.getMyTestAction = function()
{
};
commonmenuController.prototype.SideMenuOpen = function()
{
	this.dom.addClass('slide-view-menu');
};
commonmenuController.prototype.SideMenuClose = function()
{
	this.dom.removeClass('slide-view-menu');
};
commonmenuController.prototype.MenuItemActive = function(menuItem)
{
	$(".menu-item li").removeClass('active');
    $(menuItem).addClass('active');
    $(".menu-item li").css("pointer-events","auto");
    if(menuItem.data('event-none') == true){
		this.MenuItemEvent(menuItem);
	}
};
/* Pointer-event disable for clicked menu item */
commonmenuController.prototype.MenuItemEvent = function(menuItem)
{
	$(menuItem).css("pointer-events","none");
};

commonmenuController.prototype.PopulateMenu = function()
{
};