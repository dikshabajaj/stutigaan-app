var commonsidemenuController = function(data)
{
	this.view = "default";
	this.style = "default";
	this.dom = null;
	this.forView = null;
};
commonsidemenuController.prototype.Init = function(viewDom)
{
	this.dom = viewDom;
	this.dom.find('.view-blocker').unbind().bind('click',function(){
		UrlMgmt.getObj().sideMenuAction.controller.CloseView();
	});
};
commonsidemenuController.prototype.LoadSideMenu = function()
{
	var urlMgmt = UrlMgmt.getObj();
	
    /*$(".nav-swipe").swipe( {
        swipeLeft:function(event, direction, distance, duration, fingerCount) {
            this.CloseView();
        }.bind(this),
        swipeRight:function(event, direction, distance, duration, fingerCount) {
            this.OpenView();
        }.bind(this)
    });*/
};
commonsidemenuController.prototype.OpenView = function()
{
	var urlMgmt = UrlMgmt.getObj();
	this.dom.show();
	//this.LoadSideMenu();
	//urlMgmt.headerAction.controller.SideMenuOpen();
	//urlMgmt.menuAction.controller.SideMenuOpen();
	this.forView.addClass('slide-view');
	$('.view-blocker').show();
	this.BindEvents();
};
commonsidemenuController.prototype.CloseView = function()
{
	var urlMgmt = UrlMgmt.getObj();
	this.dom.hide();
	//urlMgmt.headerAction.controller.SideMenuClose();
	//urlMgmt.menuAction.controller.SideMenuClose();
	if(this.forView != null)
		this.forView.removeClass('slide-view');
	$('.view-blocker').hide();	
};
commonsidemenuController.prototype.BindEvents = function()
{
	
	$('.smenu-options').find('.js-aboutus').unbind().bind('click', function(){
		console.log("Open About us");
		UrlMgmt.getObj().sideMenuAction.controller.CloseView();
		UrlMgmt.getObj().Route({route: "common/aboutus"});
	});
	$('.smenu-options').find('.js-share').unbind().bind('click', function(){
		console.log("Open Share");
		window.plugins.socialsharing.share("Download the Stutigaan app here :\n https://play.google.com/store/apps/details?id=com.lyricsapp.stutigaan&hl=en");
	});
	$('.smenu-options').find('.js-rateapp').unbind().bind('click', function(){
		console.log("Open Rate App");
		cordova.plugins.market.open('com.lyricsapp.stutigaan');
	});
	$('.smenu-options').find('.js-contact').unbind().bind('click', function(){
		console.log("Open Contact Us");
		UrlMgmt.getObj().sideMenuAction.controller.CloseView();
		UrlMgmt.getObj().Route({route: "common/contactus"});
	});
	// $('.smenu-options').find('.js-playaudio').unbind().bind('click', function(){
	// 	console.log("Play Audio");
	// 	UrlMgmt.getObj().sideMenuAction.controller.CloseView();
	// 	playMP3();
		
		
	// });
	
};

function getFullMediaURL(s) {
    return cordova.file.applicationDirectory + 'www/Audio/audio1.mp3'
  }

  function playMP3() {
    let src = getFullMediaURL();
    var myMedia =
      new Media(src,
        function () { },
        function (e) { alert('Media Error: ' + JSON.stringify(e)); }
      );
	console.log("Audio duration: "+myMedia.getDuration());
    myMedia.play();
    myMedia.setVolume('1.0');
  }