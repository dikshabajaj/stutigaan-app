/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


//app.initialize();
$(document).ready(function(){
	//setTimeout(function(){
		//$('.preloader').hide();
		UrlMgmt.getObj();
		
		/*$.ajax({
			url:'readme.txt',
			success: function (data){
				var line = '';
				var lines = [];
				var songFound = false;
				var sno = 0;
				var html = "";
				var continueSong = false;
				for(var i=0;i<data.length;i++){	
					if(data[i] == '\n'){
						lines.push(line.trim());
						line = "";
					}
					line += data[i];
				}
				//console.log(lines);
				for(var i in lines){
					if(songFound == true){
						html += "<div data-action='song_"+sno+"' class='playsong song_"+sno+"'><h4>"+sno+"</h4><pre>"+lines[i]+"</pre></div>";
						songFound = false;
					}
					else if(lines[i].startsWith('sno') || lines[i].startsWith(' sno')){
						songFound = true;
						sno = lines[i].split('-')[1]; 
					}
					else{
						continue;
					}
				}
				$('.content').html(html);
				$('.content').find('div.playsong').unbind().bind('click', function(e){
					var songId = ($(e.currentTarget).attr('data-action')).split('_')[1];
					//Play the song
					var htmlData = "<div class='song-text'>";
					for(var i in lines){
						if(songFound == true && !lines[i].startsWith('--endofsong--')){
							htmlData += "<pre>"+lines[i]+"</pre>";
						}
						else if(songFound == true && lines[i].startsWith('--endofsong--')){
							htmlData += "</div>";
							songFound = false;
							break;
						}
						else if(lines[i].startsWith('sno')){
							var id = lines[i].split('-')[1];
							if(id == songId){
								songFound = true;
							}					
						}
						else{
							continue;
						}
					}
					$('.song-play').html(htmlData);
				});
			}
		});*/
	//}, 2000);
});